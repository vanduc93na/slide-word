// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Unlit/Textured Colored Transparent Scroll"
{
	Properties
	{
		_TintColor("Main Color", Color) = (1,1,1,1)
		_MainTex("Base (RGB)", 2D) = "white" {}
		_TextureScrollUVCenterAlphaEdgeAlpha("Texture Scroll (U.V.CenterAlpha.EdgeAlpha)", Vector) = (0,0,4,0)
		[Toggle]_UGradientSwitch("U Gradient Switch", Float) = 0
		[Toggle]_VGradientSwitch("V Gradient Switch", Float) = 0

	}
	
	SubShader
	{
		
		
		Tags { "RenderType"="Transparent" "Queue"="Transparent" }
	LOD 100

		CGINCLUDE
		#pragma target 3.0
		ENDCG
		Blend SrcAlpha OneMinusSrcAlpha
		AlphaToMask Off
		Cull Back
		ColorMask RGBA
		ZWrite On
		ZTest LEqual
		Offset 0 , 0
		
		
		
		Pass
		{
			Name "Unlit"
			Tags { "LightMode"="ForwardBase" }
			CGPROGRAM

			

			#ifndef UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX
			//only defining to not throw compilation error over Unity 5.5
			#define UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input)
			#endif
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_instancing
			#include "UnityCG.cginc"
			#include "UnityShaderVariables.cginc"
			#define ASE_NEEDS_FRAG_COLOR


			struct appdata
			{
				float4 vertex : POSITION;
				float4 color : COLOR;
				float4 ase_texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};
			
			struct v2f
			{
				float4 vertex : SV_POSITION;
				#ifdef ASE_NEEDS_FRAG_WORLD_POSITION
				float3 worldPos : TEXCOORD0;
				#endif
				float4 ase_texcoord1 : TEXCOORD1;
				float4 ase_color : COLOR;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			uniform sampler2D _MainTex;
			uniform float4 _TextureScrollUVCenterAlphaEdgeAlpha;
			uniform float4 _TintColor;
			uniform float _UGradientSwitch;
			uniform float _VGradientSwitch;

			
			v2f vert ( appdata v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
				UNITY_TRANSFER_INSTANCE_ID(v, o);

				o.ase_texcoord1.xy = v.ase_texcoord.xy;
				o.ase_color = v.color;
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.ase_texcoord1.zw = 0;
				float3 vertexValue = float3(0, 0, 0);
				#if ASE_ABSOLUTE_VERTEX_POS
				vertexValue = v.vertex.xyz;
				#endif
				vertexValue = vertexValue;
				#if ASE_ABSOLUTE_VERTEX_POS
				v.vertex.xyz = vertexValue;
				#else
				v.vertex.xyz += vertexValue;
				#endif
				o.vertex = UnityObjectToClipPos(v.vertex);

				#ifdef ASE_NEEDS_FRAG_WORLD_POSITION
				o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
				#endif
				return o;
			}
			
			fixed4 frag (v2f i ) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(i);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(i);
				fixed4 finalColor;
				#ifdef ASE_NEEDS_FRAG_WORLD_POSITION
				float3 WorldPosition = i.worldPos;
				#endif
				float2 appendResult107 = (float2(_TextureScrollUVCenterAlphaEdgeAlpha.x , _TextureScrollUVCenterAlphaEdgeAlpha.y));
				float2 panner97 = ( 1.0 * _Time.y * appendResult107 + i.ase_texcoord1.xy);
				float4 tex2DNode90 = tex2D( _MainTex, panner97 );
				float3 appendResult83 = (float3(tex2DNode90.r , tex2DNode90.g , tex2DNode90.b));
				float3 appendResult84 = (float3(_TintColor.r , _TintColor.g , _TintColor.b));
				float3 appendResult81 = (float3(i.ase_color.r , i.ase_color.g , i.ase_color.b));
				float2 break102 = abs( ( ( i.ase_texcoord1.xy * float2( 2,2 ) ) + float2( -1,-1 ) ) );
				float CenterAlpha116 = _TextureScrollUVCenterAlphaEdgeAlpha.z;
				float EdgeAlpha124 = _TextureScrollUVCenterAlphaEdgeAlpha.w;
				float4 appendResult89 = (float4(( appendResult83 * appendResult84 * appendResult81 ) , ( tex2DNode90.a * _TintColor.a * i.ase_color.a * saturate( (CenterAlpha116 + (max( (( _UGradientSwitch )?( break102.x ):( 0.0 )) , (( _VGradientSwitch )?( break102.y ):( 0.0 )) ) - 0.0) * (EdgeAlpha124 - CenterAlpha116) / (1.0 - 0.0)) ) )));
				float4 clampResult76 = clamp( appendResult89 , float4( 0,0,0,0 ) , float4( 1,1,1,1 ) );
				
				
				finalColor = clampResult76;
				return finalColor;
			}
			ENDCG
		}
	}
	CustomEditor "ASEMaterialInspector"
	

	
}
/*ASEBEGIN
Version=18909
202.6667;1446;1920;1053;1716.047;1356.098;1;True;True
Node;AmplifyShaderEditor.TexCoordVertexDataNode;92;-1586.18,-1414.294;Inherit;False;0;2;0;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;120;-1340.486,-611.8353;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;2,2;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;122;-1214.395,-610.1498;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;-1,-1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.AbsOpNode;104;-1091.201,-610.9406;Inherit;False;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.Vector4Node;106;-1632.695,-1274.599;Inherit;False;Property;_TextureScrollUVCenterAlphaEdgeAlpha;Texture Scroll (U.V.CenterAlpha.EdgeAlpha);2;0;Create;True;0;0;0;False;0;False;0,0,4,0;0,0,4,0;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.BreakToComponentsNode;102;-975.4504,-612.8604;Inherit;False;FLOAT2;1;0;FLOAT2;0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.RegisterLocalVarNode;116;-1299.918,-1141.531;Inherit;False;CenterAlpha;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;107;-1303.957,-1242.884;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.ToggleSwitchNode;134;-822.5864,-522.1919;Inherit;False;Property;_VGradientSwitch;V Gradient Switch;4;0;Create;True;0;0;0;False;0;False;0;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ToggleSwitchNode;133;-826.197,-635.0573;Inherit;False;Property;_UGradientSwitch;U Gradient Switch;3;0;Create;True;0;0;0;False;0;False;0;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;124;-1298.08,-1054.087;Inherit;False;EdgeAlpha;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;115;-559.0876,-407.9552;Inherit;False;116;CenterAlpha;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMaxOpNode;119;-496.6279,-513.6028;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;97;-855.8396,-1405.685;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.GetLocalVarNode;125;-551.7372,-317.3767;Inherit;False;124;EdgeAlpha;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.VertexColorNode;79;-543.6463,-816.3316;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;77;-574.0174,-1003.027;Inherit;False;Property;_TintColor;Main Color;0;0;Create;False;0;0;0;False;0;False;1,1,1,1;1,1,1,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;90;-649.6566,-1249.08;Inherit;True;Property;_MainTex;Base (RGB);1;0;Create;False;0;0;0;False;0;False;-1;None;2c6536772776dd84f872779990273bfc;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TFHCRemapNode;123;-348.0247,-520.8126;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;0;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;84;-341.5371,-971.8955;Inherit;False;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DynamicAppendNode;81;-336.4542,-788.3074;Inherit;False;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SaturateNode;117;-156.6131,-516.1006;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;83;-342.4451,-1208.859;Inherit;False;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;86;-112.3289,-992.155;Inherit;False;3;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;88;-128.6962,-862.6603;Inherit;False;4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;89;72.17005,-991.4785;Inherit;False;FLOAT4;4;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleTimeNode;95;-1044.356,-1161.904;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RotatorNode;93;-849.5806,-1212.294;Inherit;False;3;0;FLOAT2;0,0;False;1;FLOAT2;0.5,0.5;False;2;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.ClampOpNode;76;231.4822,-986.8998;Inherit;False;3;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;2;FLOAT4;1,1,1,1;False;1;FLOAT4;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;51;395.1846,-978.4639;Float;False;True;-1;2;ASEMaterialInspector;100;1;Unlit/Textured Colored Transparent Scroll;0770190933193b94aaa3065e307002fa;True;Unlit;0;0;Unlit;2;True;True;2;5;False;-1;10;False;-1;0;1;False;-1;0;False;-1;True;0;False;-1;0;False;-1;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;0;False;-1;False;True;True;True;True;True;0;False;-1;False;False;False;False;False;False;False;True;False;255;False;-1;255;False;-1;255;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;False;True;1;False;-1;True;3;False;-1;True;True;0;False;-1;0;False;-1;True;2;RenderType=Transparent=RenderType;Queue=Transparent=Queue=0;True;2;0;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;1;LightMode=ForwardBase;False;0;;1;=;0;Standard;1;Vertex Position,InvertActionOnDeselection;1;0;1;True;False;;False;0
WireConnection;120;0;92;0
WireConnection;122;0;120;0
WireConnection;104;0;122;0
WireConnection;102;0;104;0
WireConnection;116;0;106;3
WireConnection;107;0;106;1
WireConnection;107;1;106;2
WireConnection;134;1;102;1
WireConnection;133;1;102;0
WireConnection;124;0;106;4
WireConnection;119;0;133;0
WireConnection;119;1;134;0
WireConnection;97;0;92;0
WireConnection;97;2;107;0
WireConnection;90;1;97;0
WireConnection;123;0;119;0
WireConnection;123;3;115;0
WireConnection;123;4;125;0
WireConnection;84;0;77;1
WireConnection;84;1;77;2
WireConnection;84;2;77;3
WireConnection;81;0;79;1
WireConnection;81;1;79;2
WireConnection;81;2;79;3
WireConnection;117;0;123;0
WireConnection;83;0;90;1
WireConnection;83;1;90;2
WireConnection;83;2;90;3
WireConnection;86;0;83;0
WireConnection;86;1;84;0
WireConnection;86;2;81;0
WireConnection;88;0;90;4
WireConnection;88;1;77;4
WireConnection;88;2;79;4
WireConnection;88;3;117;0
WireConnection;89;0;86;0
WireConnection;89;3;88;0
WireConnection;93;0;92;0
WireConnection;93;2;95;0
WireConnection;76;0;89;0
WireConnection;51;0;76;0
ASEEND*/
//CHKSM=7B195C0907B5C0ACA37DB817C3754C4615E63161