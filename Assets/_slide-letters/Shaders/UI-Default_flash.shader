Shader "UI/Default_flash"
{
    Properties
    {
        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_FlashTex("Flash Texture", 2D) = "black" {}

		[PowerSlider(2.5)]_FlashSpeedX("_FlashSpeedX", Range(0, 10)) = 1.0
		[IntRange]_FlashInvereseX("_FlashInvereseX", Range(0, 1)) = 0
		[PowerSlider(2.5)]_FlashSpeedY("_FlashSpeedY", Range(0, 10)) = 1.0
		[IntRange]_FlashInvereseY("_FlashInvereseY", Range(0, 1)) = 0
		[IntRange]_loopCycle("Loop Cycle", Range(1,10)) = 5.0
		[Space(15)]

        _Color ("Tint", Color) = (1,1,1,1)
		_Alpha("_Alpha", Range(0, 1)) = 1.0

        _StencilComp ("Stencil Comparison", Float) = 8
        _Stencil ("Stencil ID", Float) = 0
        _StencilOp ("Stencil Operation", Float) = 0
        _StencilWriteMask ("Stencil Write Mask", Float) = 255
        _StencilReadMask ("Stencil Read Mask", Float) = 255

        _ColorMask ("Color Mask", Float) = 15

        [Toggle(UNITY_UI_ALPHACLIP)] _UseUIAlphaClip ("Use Alpha Clip", Float) = 0
    }

    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }

        Stencil
        {
            Ref [_Stencil]
            Comp [_StencilComp]
            Pass [_StencilOp]
            ReadMask [_StencilReadMask]
            WriteMask [_StencilWriteMask]
        }

        Cull Off
        Lighting Off
        ZWrite Off
        ZTest [unity_GUIZTestMode]
        Blend SrcAlpha OneMinusSrcAlpha
        ColorMask [_ColorMask]

        Pass
        {
            Name "Default"
        CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 2.0

            #include "UnityCG.cginc"
            #include "UnityUI.cginc"

            #pragma multi_compile_local _ UNITY_UI_CLIP_RECT
            #pragma multi_compile_local _ UNITY_UI_ALPHACLIP

            struct appdata_t
            {
                float4 vertex   : POSITION;
                float4 color    : COLOR;
                float2 texcoord : TEXCOORD0;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct v2f
            {
                float4 vertex   : SV_POSITION;
                fixed4 color    : COLOR;
                float2 texcoord  : TEXCOORD0;
                float4 worldPosition : TEXCOORD1;
				float2 texcoord2  : TEXCOORD2;
                UNITY_VERTEX_OUTPUT_STEREO
            };

            sampler2D _MainTex;
			sampler2D _FlashTex;
			float4 _FlashTex_ST;
            fixed4 _Color;
			float _Alpha;
            fixed4 _TextureSampleAdd;
            float4 _ClipRect;
            float4 _MainTex_ST;
			float _FlashSpeedX;
			float _FlashSpeedY;
			int _FlashInvereseX;
			int _FlashInvereseY;
			int _loopCycle;

            v2f vert(appdata_t v)
            {
                v2f OUT;
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);
                OUT.worldPosition = v.vertex;
                OUT.vertex = UnityObjectToClipPos(OUT.worldPosition);

                OUT.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);
				OUT.texcoord2 = TRANSFORM_TEX(v.texcoord, _FlashTex);

                OUT.color = v.color * _Color;
                return OUT;
            }

            fixed4 frag(v2f IN) : SV_Target
            {
                half4 color = (tex2D(_MainTex, IN.texcoord) + _TextureSampleAdd) * IN.color;

                #ifdef UNITY_UI_CLIP_RECT
                color.a *= UnityGet2DClipping(IN.worldPosition.xy, _ClipRect);
                #endif

                #ifdef UNITY_UI_ALPHACLIP
                clip (color.a - 0.001);
                #endif

				//uv1.x = i.uv1.x - (_Time.x * 10.0 * BPS_Rate * _Light_R_SpeedX * _Light_R_InvereseX1);
				//uv1.y = i.uv1.y - (_Time.x * 10.0 * BPS_Rate * _Light_R_SpeedY * _Light_R_InvereseY1);

				float2 uv = IN.texcoord2;

				_FlashInvereseX = (_FlashInvereseX - 0.5) * 2.0;//0or1を-1or1に変える
				_FlashInvereseY = (_FlashInvereseY - 0.5) * 2.0;

				//リファクタ対象
				uv.x = uv.x - (_Time.x * 10.0 * _FlashSpeedX * _FlashInvereseX);//速度調整
				uv.y = uv.y - (_Time.x * 10.0 * _FlashSpeedY * _FlashInvereseY);

				//リファクタ対象
				float uvCheck = floor(uv.x);
				uvCheck = fmod(uvCheck, _loopCycle);
				uvCheck = uvCheck == 0 ? 1 : 0;

				fixed4 flashCol = tex2D(_FlashTex, uv);
				color.rgb = color.rgb + (flashCol.rgb * uvCheck * _Alpha);

                return color;
            }
        ENDCG
        }
    }
}
