﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class WriteTextFileToScriptableObject : MonoBehaviour
{
    [ContextMenu("write data")]
    public void WriteHint()
    {
        string path = "Assets/_slide-letters/Utilities/MakeHint/hints.txt";
        StreamReader reader = new StreamReader(path);
        List<string> hints = new List<string>();
        while (!reader.EndOfStream)
        {
            string t = reader.ReadLine();
            hints.Add(t);
        }
        HintConfig.SetHint(hints);
    }
}
