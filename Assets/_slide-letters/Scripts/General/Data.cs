﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Data
{

    private static int _CurrentLevel = PlayerPrefs.GetInt("CurrentLevel", 1);
    private static int _CurrentCoin = PlayerPrefs.GetInt("CurrentCoin");
    public static int _TotalLevel = 14;
    public static string Prd_TOTAL_HINT = "word.hint";
    public static string Prd_NOADS = "word.noads";
    public static string Prd_HINT_TOPIC = "word.hint.topic";

    private static int _UpgradeSpeedLevel = PlayerPrefs.GetInt("UpgradeSpeedLevel");
    private static int _UpgradePowerLevel = PlayerPrefs.GetInt("UpgradePowerLevel");
    private static int _UpgradeInkLevel = PlayerPrefs.GetInt("UpgradeInkLevel");
    private static int _UpgradeDefenseLevel = PlayerPrefs.GetInt("UpgradeDefenseLevel");
    private static List<string> _hints = HintConfig.GetHints();

    public static void Clear()
    {
        PlayerPrefs.DeleteAll();
        PlayerPrefs.Save();

        _CurrentLevel = 1;
        _CurrentCoin = 0;

        _UpgradeSpeedLevel = 0;
        _UpgradePowerLevel = 0;
        _UpgradeInkLevel = 0;
    }

    public static string CurrentHint
    {
        get
        {
            return _hints[_CurrentLevel];
        }

        set
        {
            _hints = HintConfig.GetHints();
        }
    }

    public static bool IsSoundOn
    {
        get
        {
            return PlayerPrefs.GetInt("IsSoundOn", 1) == 1;
        }
        set
        {
            PlayerPrefs.SetInt("IsSoundOn", value ? 1 : 0);
            PlayerPrefs.Save();
        }
    }

    public static bool IsVibrationOn
    {
        get
        {
            return PlayerPrefs.GetInt("IsVibrationOn", 1) == 1;
        }
        set
        {
            PlayerPrefs.SetInt("IsVibrationOn", value ? 1 : 0);
            PlayerPrefs.Save();
        }
    }

    public static int UpgradeDefenseLevel
    {
        get
        {
            return _UpgradeDefenseLevel;
        }
        set
        {
            _UpgradeDefenseLevel = value;
            PlayerPrefs.SetInt("UpgradeDefenseLevel", value);
            PlayerPrefs.Save();
        }
    }

    public static bool IsNoADS()
    {
        return PlayerPrefs.HasKey(Prd_NOADS);
    }

    public static int TotalHint
    {
        get { return PlayerPrefs.GetInt(Prd_TOTAL_HINT, 0); }
        set
        {
            int result = PlayerPrefs.GetInt(Prd_TOTAL_HINT, 0);
            result += value;
            PlayerPrefs.SetInt(Prd_TOTAL_HINT, result);
        }
    }

    public static bool IsShowHintTopic()
    {
        return PlayerPrefs.HasKey(Prd_HINT_TOPIC);
    }

    public static int CurrentLevel
    {
        get
        {
            return _CurrentLevel;
        }
        set
        {
            _CurrentLevel = value;
            if (_CurrentLevel > _TotalLevel) _CurrentLevel = 1;
            PlayerPrefs.SetInt("CurrentLevel", _CurrentLevel);
            PlayerPrefs.Save();
        }
    }
    
    public static int CurrentCoin {
        get {
            return _CurrentCoin;
        }
        set {
            _CurrentCoin = value;
            PlayerPrefs.SetInt("CurrentCoin", value);
            PlayerPrefs.Save();
        }
    }

    public static int UpgradeSpeedLevel
    {
        get
        {
            return _UpgradeSpeedLevel;
        }
        set
        {
            _UpgradeSpeedLevel = value;
            PlayerPrefs.SetInt("UpgradeSpeedLevel", value);
            PlayerPrefs.Save();
        }
    }

    public static int UpgradePowerLevel
    {
        get
        {
            return _UpgradePowerLevel;
        }
        set
        {
            _UpgradePowerLevel = value;
            PlayerPrefs.SetInt("UpgradePowerLevel", value);
            PlayerPrefs.Save();
        }
    }

    public static int UpgradeInkLevel
    {
        get
        {
            return _UpgradeInkLevel;
        }
        set
        {
            _UpgradeInkLevel = value;
            PlayerPrefs.SetInt("UpgradeInkLevel", value);
            PlayerPrefs.Save();
        }
    }

    public static int Coin
    {
        get
        {
            return PlayerPrefs.GetInt("COIN", 0);
        }
        set
        {
            int currentCoin = PlayerPrefs.GetInt("COIN", 0);
            int result = currentCoin + value;
            PlayerPrefs.SetInt("COIN", result);
        }
    }
}
