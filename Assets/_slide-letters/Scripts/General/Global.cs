﻿using MoreMountains.NiceVibrations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class Global : MonoBehaviour
{
    private static EventSystem eventSystem;

    public static bool _isSoundOn, _isVibrationOn;
    public static int _curLevel, _totalLevel;

    private void Awake()
    {
        if (!eventSystem) eventSystem = FindObjectOfType<EventSystem>();
        _isSoundOn = Data.IsSoundOn;
        _isVibrationOn = Data.IsVibrationOn;
        _curLevel = Data.CurrentLevel;
    }
    
    public static void VibrationHaptic(int TypeNum)
	{
		if (!Data.IsVibrationOn) return;
        switch (TypeNum)
        {
            case 1:
                MMVibrationManager.Haptic(HapticTypes.Success);
                break;
            case 2:
                MMVibrationManager.Haptic(HapticTypes.Warning);
                break;
            case 3:
                MMVibrationManager.Haptic(HapticTypes.Failure);
                break;
            case 4:
                MMVibrationManager.Haptic(HapticTypes.LightImpact);
                break;
            case 5:
                MMVibrationManager.Haptic(HapticTypes.MediumImpact);
                break;
            case 6:
                MMVibrationManager.Haptic(HapticTypes.HeavyImpact);
                break;
            /*case 7:
				MMVibrationManager.Haptic(HapticTypes.RigidImpact);
				break;
			case 8:
				MMVibrationManager.Haptic(HapticTypes.SoftImpact);
				break;*/
            default:
                MMVibrationManager.Haptic(HapticTypes.Selection);
                break;
        }
    }

	public static void LoadScene(string SceneName)
    {
		SceneManager.LoadScene(SceneName);
	}

    public static void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public static IEnumerator Delay(float seconds, System.Action Callback)
    {
        yield return new WaitForSeconds(seconds);
        Callback();
    }

    public static bool CheckTouchOnUI()
    {
#if UNITY_EDITOR
        return eventSystem.IsPointerOverGameObject(-1);
#else
        if (Input.touchCount == 0) return false;
        return eventSystem.IsPointerOverGameObject(Input.GetTouch(0).fingerId);
#endif
    }
}
