﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "HintsConfig", menuName = "Create Hints")]
public class HintConfig : ScriptableObject
{
    public List<string> hints;

    public static List<string> GetHints()
    {
        var hintConfig = Resources.Load<HintConfig>("Hints/HintsConfig");
        return hintConfig.hints;
    }

    public static void SetHint(List<string> hintsLst)
    {
        var hintConfig = Resources.Load<HintConfig>("Hints/HintsConfig");
        hintConfig.hints = hintsLst;
#if UNITY_EDITOR
        AssetDatabase.SaveAssets();
#endif
    }
}
