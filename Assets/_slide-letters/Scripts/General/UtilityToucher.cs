﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UtilityToucher
{
    private static EventSystem eventSystem;

    public static bool GetMouse() => Application.isEditor ? Input.GetMouseButton(0) : Input.touchCount > 0;
    public static bool GetMouseDown() => Application.isEditor ? Input.GetMouseButtonDown(0) : Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began;
    public static bool GetMouseUp() => Application.isEditor ? Input.GetMouseButtonUp(0) : Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended;
    public static Vector3 GetMousePosition() => Application.isEditor || Input.touchCount == 0 ? Input.mousePosition : (Vector3)Input.GetTouch(0).position;

    public static bool IsTouchOnUI()
    {
        if (!eventSystem) eventSystem = Object.FindObjectOfType<EventSystem>();
#if UNITY_EDITOR
        return eventSystem.IsPointerOverGameObject(-1);
#else
                return Input.touchCount > 0 && eventSystem.IsPointerOverGameObject(Input.GetTouch(0).fingerId);
#endif
    }
}
