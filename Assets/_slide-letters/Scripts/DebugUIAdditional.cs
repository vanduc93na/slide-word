﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using HyperCasualTemplate;

public class DebugUIAdditional : MonoBehaviour
{
    //Public
    public static DebugUIAdditional instance;

    //Private
    private AppLovinMaxSDK maxSDK;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        maxSDK = (AppLovinMaxSDK)FindObjectOfType(typeof(AppLovinMaxSDK));
    }

    public void ToggleBannerVisibility()
    {
        // デバッグボタンに本スクリプトがAddされている->開発ROMではデバッグボタンが無効->Startが呼ばれない。なのでここで代入する
        if (!maxSDK)
        {
            maxSDK = (AppLovinMaxSDK)FindObjectOfType(typeof(AppLovinMaxSDK));
        }

        if (maxSDK) maxSDK.ToggleBannerVisibility();
    }
}


public partial class SROptions
{
    // private const string OtherSettingsCategory = "Other Settings";
    // private int NumberCoins = Data.CurrentCoin;

    [Category(GeneralCategory)]
    [DisplayName("Next Level [+]")]
    [Sort(951)]
    public void NextLevel()
    {
        if (Data.CurrentLevel == Data._TotalLevel) Data.CurrentLevel = 1;
        else Data.CurrentLevel++;
        Global.ReloadScene();
    }

    [Category(GeneralCategory)]
    [DisplayName("Previous Level [-]")]
    [Sort(952)]
    public void PreviousLevel()
    {
        if (Data.CurrentLevel == 1) Data.CurrentLevel = Data._TotalLevel;
        else Data.CurrentLevel--;
        Global.ReloadScene();
    }

    [Category(GeneralCategory)]
    [DisplayName("Restart This Level")]
    [Sort(953)]
    public void RestartThisLevel()
    {
        Global.ReloadScene();
    }

    [Category(GeneralCategory)]
    [DisplayName("Select Level")]
    [Sort(954)]
    public int InputLevel
    {
        get { return Data.CurrentLevel; }
        set
        {
            Data.CurrentLevel = Mathf.Max(value, 1);
            Global.ReloadScene();
        }
    }

    [Category(GeneralCategory)]
    [DisplayName("Next Stage")]
    [Sort(956)]
    public void NextStage()
    {
        GameManager.instance.Debug_NextStage();
    }

    /*
    [Category(GeneralCategory)]
    [DisplayName("Add Coins")]
    [Sort(961)]
    public int InputAddCoins
    {
        get { return NumberCoins; }
        set
        {
            NumberCoins = value;
            UICoin.instance.UpdateCoin(value);
        }
    }*/

    [Category(GeneralCategory)]
    [DisplayName("Toggle Banner Ad")]
    [Sort(957)]
    public void ToggleBannerVisibility()
    {
        DebugUIAdditional.instance.ToggleBannerVisibility();
    }

    [Category(GeneralCategory)]
    [DisplayName("Show Mediation Debugger")]
    [Sort(958)]
    public void _ShowMediationDebugger()
    {
        //if (maxSDK) 
        MaxSdk.ShowMediationDebugger();
    }
    /*
    [Category(GeneralCategory)]
    [DisplayName("Send notification after 5s")]
    [Sort(959)]
    public void SendNotification()
    {
        HyperCasualTemplate.MobileNotifications.instance.AddNotificationSchedule(5);
    }*/

    [Category(GeneralCategory)]
    [DisplayName("!! Reset Game Data !!")]
    [Sort(3000)]
    public void ResetGameData()
    {
        Data.Clear();
        Global.ReloadScene();
    }
}