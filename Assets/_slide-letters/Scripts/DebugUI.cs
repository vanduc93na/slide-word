﻿using System.Collections;
using System.Collections.Generic;
using HyperCasualTemplate;
#if !DISABLE_SRDEBUGGER
using SRDebugger;
#endif
using UnityEngine;

public class DebugUI : MonoBehaviour
{
    void Start()
    {
#if (!UNITY_EDITOR) || (NO_UIDEBUG) || (DISABLE_SRDEBUGGER)
        gameObject.SetActive(false);
#endif
    }

    public void Open()
    {
        ConfigData.OpenSRDebuggerWindow();
    }
}
