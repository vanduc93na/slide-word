﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HintOrder : MonoBehaviour
{
    public List<string> hints = new List<string>();

    public List<string> GetHints()
    {
        return hints;
    }
}
