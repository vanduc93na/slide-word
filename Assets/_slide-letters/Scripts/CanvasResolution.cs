﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum AspectRatios
{
    _default,
    _5x3,
    _3x2,
    _7x5,
    _4x3,
    _auto
}

public class CanvasResolution : MonoBehaviour
{
    public Vector2 defaultResolution = new Vector2(1080, 1920);
    //public AspectRatios aspectRatio;
    private CanvasScaler[] canvasScalers;

    private void Start()
    {
        canvasScalers = FindObjectsOfType<CanvasScaler>();
        if (canvasScalers == null || canvasScalers.Length == 0) return;

        Vector2 referenceResolution = defaultResolution;

        float aspectRatio = (float)Screen.width/(float)Screen.height;// Camera.main.aspect;// 16:9 = 0.5625
        //Debug.Log(aspectRatio);
        if (aspectRatio < .4f) return;
        if (aspectRatio >= .8f)//
            referenceResolution = new Vector2(1920f, 1920f);// 1.777
        else if(aspectRatio >= .75f)// 4:3 = 0.75
            referenceResolution = new Vector2(1800f, 1920f);// 0.781
        else if (aspectRatio >= .714f)// 7:5 ~= 0.714
            referenceResolution = new Vector2(1600f, 1920f);
        else if (aspectRatio >= .666f)// 3:2 ~= 0.666
            referenceResolution = new Vector2(1500f, 1920f);
        else if (aspectRatio >= .6f)// 5:3 = 0.6
            referenceResolution = new Vector2(1400f, 1920f);
        else if (aspectRatio >= .55f)
            referenceResolution = new Vector2(1350f, 1920f);
        else if (aspectRatio >= .5f)
            referenceResolution = new Vector2(1300f, 1920f);
        else if (aspectRatio >= .45f)
            referenceResolution = new Vector2(1200f, 1920f);
        else if (aspectRatio >= .4f)
            referenceResolution = new Vector2(1150f, 1920f);

        foreach (CanvasScaler scaler in canvasScalers)
            scaler.referenceResolution = referenceResolution;
    }
    /*
#if UNITY_EDITOR
    private void Update()
    {
        if (canvasScalers == null || canvasScalers.Length == 0) return;

        Vector2 referenceResolution = defaultResolution;
        switch (aspectRatio)
        {
            case AspectRatios._7x5:
                referenceResolution = new Vector2(1424f, 2320f);
                break;
            case AspectRatios._5x3:
                referenceResolution = new Vector2(1240f, 1800f);
                break;
            case AspectRatios._4x3:
                referenceResolution = new Vector2(1500f, 2048f);
                break;
            case AspectRatios._3x2:
                referenceResolution = new Vector2(1350f, 2160f);
                break;
            default:
                referenceResolution = defaultResolution;
                break;
        }

        foreach (CanvasScaler scaler in canvasScalers)
            scaler.referenceResolution = referenceResolution;
    }
#endif
*/
}
