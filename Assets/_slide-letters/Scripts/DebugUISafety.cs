﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugUISafety : MonoBehaviour
{
    public GameObject[] disableObjects;

    void Awake()
    {
        // If disableObjects is not set to anything, it will automatically set its own GameObject.
        // もしもdisableObjectsに何も設定されていないなら、自動的に自分自身のGameObjectをセットする
        if (disableObjects == null || disableObjects.Length == 0)
        {
            disableObjects = new GameObject[1];
            disableObjects[0] = this.gameObject;
        }
#if NO_UIDEBUG
        if(disableObjects != null)
        {
            for(var i = 0; i < disableObjects.Length; i++)
            {
                disableObjects[i].SetActive(false);
            }
        }
#endif
    }

}
