﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIDrawMaterial : MonoBehaviour
{
    //Publuc
    [Range(0, 1)]
    public float _DrawProgress = 1f;
    [Range(0, 1)]
    public float _EraseProgress = 0f;
    [Range(0, 1)]
    public float _Alpha = 1f;

    //Private
    private Image image;
    private Material mat;

    private void Start()
    {
        image = GetComponent<Image>();
        mat = new Material(image.material.shader);
        mat.CopyPropertiesFromMaterial(image.material);
        image.material = mat;
    }

    private void Update()
    {
        mat.SetFloat("_DrawProgress", _DrawProgress);
        mat.SetFloat("_EraseProgress", _EraseProgress);
        mat.SetFloat("_Alpha", _Alpha);
    }
}
