﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIRoot : MonoBehaviour
{
    public TMPro.TextMeshProUGUI Num_Level;
    public CanFailDisplay CanFailDisplay;
    public UIHeader header;
}
