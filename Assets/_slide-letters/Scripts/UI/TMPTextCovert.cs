﻿using System;

public static class TMPTextCovert
{
    // SpriteAssets変換用
    public static string ConvFont(string str, string textColor = "")
    {
        string rtnStr = "";
        // NULLの場合対象外
        if (str == null)
            return rtnStr;
        // 文字列を一文字ずつ変換
        for (int i = 0; i < str.Length; i++)
        {
            // 文字列変換
            string convStr;

            //ColorCode
            string color = "";

            #region SampleLevel
            /*
            /// <summary>
            /// Level、の文字を作った場合のサンプル記述
            /// </summary>
            switch (str[i])
            {
                case ' ':
                    convStr = "10";
                    break;
                case 'L':
                    convStr = "11";
                    break;
                case 'e':
                    convStr = "12";
                    break;
                case 'v':
                    convStr = "13";
                    break;
                case 'l':
                    convStr = "14";
                    break;
                // 上記以外（0〜9はそのままIndexのIDに設定）
                default:
                    convStr = str[i].ToString();
                    break;
            }
            */
            #endregion

            //今回は数字のみを利用するのでこの形
            convStr = str[i].ToString();

            //文字色指定できるように追加
            if(textColor.Length > 0)
            {
                color = " color=" + textColor;
            }

            // 「<sprite=【IndexのID】>」の形に変換
            rtnStr += "<sprite=" + convStr + color + ">";
        }
        return rtnStr;
    }
}
