﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanFailDisplay : MonoBehaviour
{

    public GameObject FailIndicator;

    public int LifeMax = 3;
    public int LifeNow = 3;

    private GameObject _failIndicator_Prefab;
    private GameObject _failIndicatorBG_Prefab;
    private const string _failIndicator_String = "UI/Indicator_Point";
    private const string _failIndicatorBG_String = "UI/Indicator_BG";
    private List<GameObject> _lifeUI = new List<GameObject>();
    private Indicator_Point indexLifeUI;



    void Awake()
    {
        _failIndicator_Prefab = Resources.Load<GameObject>(_failIndicator_String);
        _failIndicatorBG_Prefab = Resources.Load<GameObject>(_failIndicatorBG_String);

        UpdateIndicatorFrame();
        UpdateRemainingLife();
    }
    
    private void DeleteAllChild(GameObject parent)
    {
        foreach (Transform child in parent.transform)
        {
            Destroy(child.gameObject);
        }

        _lifeUI.Clear();
    }

    private void UpdateIndicatorFrame()
    {
        if (FailIndicator == null)
        {
            return;
        }
        if(_failIndicator_Prefab == null || _failIndicatorBG_Prefab == null)
        {
            return;
        }
        
        DeleteAllChild(FailIndicator);

        _lifeUI.Add(Instantiate(_failIndicatorBG_Prefab, FailIndicator.transform));

        for (int i = 0; i < LifeMax; i++)
        {
            _lifeUI.Add(Instantiate(_failIndicator_Prefab, FailIndicator.transform));
        }
        
        LifeNow = LifeMax;
        UpdateLifeToNow(LifeNow);
    }


    public void UpdateRemainingLife()
    {
        //ToMarked
        for(int i= 1; i < LifeNow; i++)
        {
            UpdateLifeToMarked(i);
        }

        //ToNow
        if(LifeNow != 0)
        {
            UpdateLifeToNow(LifeNow);
        }

        //ToEmpty
        for (int i = LifeNow + 1; i < _lifeUI.Count; i++)
        {
            UpdateLifeToEmpty(i);
        }
    }

    public void DisplayFailEffect(int index)
    {
        if (!IsIndexInLifeUI(index)) return;

        indexLifeUI = _lifeUI[index].GetComponent<Indicator_Point>();

        if(indexLifeUI.FailEffect == null)
        {
            Debug.LogError("Not set an Effect on Indicator_Point");
            return;
        }
        indexLifeUI.FailEffect.Play();
    }

    private bool IsIndexInLifeUI(int index)
    {
        if (index >= _lifeUI.Count)
        {
            Debug.LogError("Out of Bound Index for MaxLife.");
            return false;
        }
        return true;
    }
    
    public void UpdateLifeToMarked(int index)
    {
        if (!IsIndexInLifeUI(index)) return;

        indexLifeUI = _lifeUI[index].GetComponent<Indicator_Point>();

        indexLifeUI.Marked.SetActive(true);
        indexLifeUI.Now.SetActive(false);
        indexLifeUI.Empty.SetActive(false);
    }

    public void UpdateLifeToNow(int index)
    {
        if (!IsIndexInLifeUI(index)) return;

        indexLifeUI = _lifeUI[index].GetComponent<Indicator_Point>();

        indexLifeUI.Marked.SetActive(false);
        indexLifeUI.Now.SetActive(true);
        indexLifeUI.Empty.SetActive(false);
    }

    public void UpdateLifeToEmpty(int index)
    {
        if (!IsIndexInLifeUI(index)) return;

        indexLifeUI = _lifeUI[index].GetComponent<Indicator_Point>();

        indexLifeUI.Marked.SetActive(false);
        indexLifeUI.Now.SetActive(false);
        indexLifeUI.Empty.SetActive(true);
    }

}
