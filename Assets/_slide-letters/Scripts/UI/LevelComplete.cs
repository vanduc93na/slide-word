﻿using System.Collections;
using System.Collections.Generic;
using HyperCasualTemplate;
using UnityEngine;
using UnityEngine.UI;

public class LevelComplete : MonoBehaviour
{
    [SerializeField] private CanFailDisplay uiRoot;
    [SerializeField] private Text remainCoinTxt;
    [SerializeField] private Text rewardCoinTxt;

    public int rewardVideoCoin = 0;
    
    void OnEnable()
    {
        long totalReward = ConfigData.GetValue<long>(Config.GetCoins_inGame_CubeLetters);
        int lifeSub = uiRoot.LifeMax - uiRoot.LifeNow;
        int remainCoin = 0;
        remainCoin = (int)totalReward - (lifeSub * 10);
        rewardVideoCoin = remainCoin * 3;
        rewardCoinTxt.text = rewardVideoCoin.ToString();
        Data.CurrentCoin += remainCoin;
        StartCoroutine(CountUpCoin(remainCoin));
    }

    IEnumerator CountUpCoin(int remainCoin)
    {
        int t = 0;
        for (int i = 0; i < 10; i++)
        {
            t += remainCoin / 10;
            remainCoinTxt.text = t.ToString();
            yield return new WaitForSeconds(0.05f);
        }

        remainCoinTxt.text = remainCoin.ToString();
    }

    public void ClickClaim()
    {
        
    }

    public void ClickContinue()
    {

    }

}
