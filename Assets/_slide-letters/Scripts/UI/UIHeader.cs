﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIHeader : MonoBehaviour
{
    [SerializeField] private GameObject hintPanel;
    [SerializeField] private Text hintTxt;
    [SerializeField] private Button okBtn;
    [SerializeField] private GameObject boadHint;
    // show hint order
    [SerializeField] private GameObject Bottom;
    [SerializeField] private GameObject hintTopic;

    [Space(30)] [Header("config")]
    [SerializeField] private HintConfig config;
    private List<string> hints = new List<string>();
    private HyperCasualTemplate.AppLovinMaxSDK maxSDK;

    void Awake()
    {
        hintPanel.SetActive(false);
//        store.SetActive(false);
        
    }

    void Start()
    {
        maxSDK = (HyperCasualTemplate.AppLovinMaxSDK) FindObjectOfType(typeof(HyperCasualTemplate.AppLovinMaxSDK));
    }

    void OnEnable()
    {
        hintTopic.SetActive(Data.IsShowHintTopic());
        Text topicTxt = hintTopic.transform.GetChild(0).GetComponent<Text>();
        topicTxt.text = config.hints[Data.CurrentLevel];
    }

    public void ShowHintOrder()
    {
        maxSDK.ShowRewardedAd((bool isSucces) =>
        {
            Data.TotalHint -= 1;
            if (!isSucces) return;
            StartCoroutine(OpenHintOrder());
            string result = "";
            for (int i = 0; i < hints.Count; i++)
            {
                result += hints[i];
                result += "\n";
            }
            hintTxt.text = result;
            hintPanel.SetActive(true);
        });
    }

    IEnumerator OpenHintOrder()
    {
        Text text = Bottom.transform.GetChild(0).GetComponent<Text>();
        string result = "";
        for (int i = 0; i < hints.Count; i++)
        {
            result += hints[i];
            if (i < hints.Count - 1)
            {
                result += "\n";
            }
        }
        text.text = result;
        Bottom.SetActive(true);
        float alpha = 0;
        text.color = new Color(1, 1, 1, alpha);
        for (int i = 0; i < 5; i++)
        {
            alpha += 0.2f;
            text.color = new Color(1, 1, 1, alpha);
            yield return new WaitForSeconds(0.15f);
        }

        yield return new WaitForSeconds(5f);

        StartCoroutine(CloseHintOrder());
    }

    IEnumerator CloseHintOrder()
    {
        Text text = Bottom.transform.GetChild(0).GetComponent<Text>();
        
        float alpha = 1f;
        for (int i = 0; i < 6; i++)
        {
            yield return new WaitForSeconds(0.15f);
            text.color = i % 2 == 0 ? new Color(1, 1, 1, 0) : new Color(1, 1, 1, 1);
        }
        text.color = new Color(1, 1, 1, 1);
        for (int i = 0; i < 10; i++)
        {
            yield return new WaitForSeconds(0.1f);
            alpha -= 0.1f;
            text.color = new Color(1, 1, 1, alpha);
        }
        Bottom.SetActive(false);
        hintPanel.SetActive(false);
        text.color = new Color(1, 1, 1, 1);
    }

    public void CloseHint()
    {
        StartCoroutine(FadeOutHint());
    }

    public void SkipStage()
    {
        maxSDK.ShowRewardedAd((bool isSucces) =>
        {
            if (!isSucces) return;
            Data.CurrentLevel += 1;
            StartCoroutine(Global.Delay(1f, ()=>{Global.ReloadScene();}));
        });
    }

    IEnumerator FadeOutHint()
    {
        okBtn.gameObject.SetActive(false);
        float scale = 1f;
        for (int i = 0; i < 5; i++)
        {
            scale += 0.02f;
            boadHint.transform.localScale = new Vector3(scale, scale);
            yield return new WaitForSeconds(0.015f);
        }

        for (int i = 0; i < 10; i++)
        {
            scale -= 0.1f;
            boadHint.transform.localScale = new Vector3(scale, scale);
            yield return new WaitForSeconds(0.02f);
        }
        boadHint.SetActive(false);
        boadHint.transform.localScale = Vector3.one;
    }

    public void ShowReward(LevelComplete levelComplete)
    {
        maxSDK.ShowRewardedAd((bool isSucces) =>
        {
            if (!isSucces) return;
            Data.CurrentCoin += levelComplete.rewardVideoCoin;
        });
    }

    public void SetHints(List<string> hints)
    {
        this.hints.Clear();
        this.hints = hints;
    }

    [ContextMenu("test")]
    public void Test()
    {
        PlayerPrefs.SetInt(Data.Prd_HINT_TOPIC, 1);
    }

}
