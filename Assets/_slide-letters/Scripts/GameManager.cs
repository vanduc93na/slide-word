﻿using HyperCasualTemplate;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum ResultMoveType
{
    Stop,
    Moving,
    GoBack,
    Complete,
    Teleport
}

public enum CheckMoveType
{
    MoveOnCharacter,
    MoveOnlyOnBlankBox
}

public class GameManager : MonoBehaviour
{
    //Public
    public static GameManager instance;
    public Transform infoCanvas;
    public UIRoot UIRoot;
    public int level;
    public bool isDebug = false;

    [SerializeField] private GameObject tutorial;
    [HideInInspector] public bool isEnded;
    [HideInInspector] public bool isWin;

    //Private
    [SerializeField] GameObject plane;
    private GameObject ResultPanel;
    private TMPro.TextMeshProUGUI LableLevel;
    private Text LableCanFail;
    private Transform MapTransform;
    private GameObject TapToStart;
    private List<Character> characters;
    private bool isStarted;
    private int countFailed = 0;
    private int canFail = 3;
    private CheckMoveType checkMoveType = CheckMoveType.MoveOnCharacter;
    private AppLovinMaxSDK maxSDK;
    private const string TUTORIAL_PREF = "tutorial";


    private void Awake()
    {
        if (ConfigData.FirebaseSDKInstance == null)
        {
            Global.LoadScene("LoadingScene");
            return;
        }
        instance = this;
    }

    private void Start()
    {
        // check is fistime ? show tutorial or not
        CheckShowTutorial();
        ResultPanel = infoCanvas.Find("ResultUIRoot").gameObject;
        LableLevel = UIRoot.Num_Level;
        LableLevel.text = TMPTextCovert.ConvFont(string.Format("{0}", Data.CurrentLevel), "#d700fc");
        LableCanFail = infoCanvas.Find("LableCanFail").GetComponentInChildren<Text>();
        UIRoot.CanFailDisplay.LifeMax = canFail;
        UIRoot.CanFailDisplay.LifeNow = canFail - countFailed;
        UIRoot.CanFailDisplay.UpdateRemainingLife();
        TapToStart = infoCanvas.Find("TapToStart").gameObject;
        int level = Data.CurrentLevel;
        if (isDebug)
        {
            level = this.level;
        }
        var levelPrefab = Resources.Load<GameObject>("Levels/Level" + level);
        List<string> hints = levelPrefab.GetComponent<HintOrder>().GetHints();
        UIRoot.header.SetHints(hints);
        MapTransform = Instantiate(levelPrefab, Vector3.zero, Quaternion.identity).transform.GetChild(0);
        maxSDK = (AppLovinMaxSDK)FindObjectOfType(typeof(AppLovinMaxSDK));
        getCharacters();
    }

    private void ShowTutorial()
    {
        tutorial.SetActive(true);
    }

    public void getCharacters()
    {
        if (!MapTransform) return;
        characters = new List<Character>();
        foreach (Transform t in MapTransform)
        {
            characters.Add(t.GetComponent<Character>());
        }
    }

    public List<Character> Characters()
    {
        if (!MapTransform) return null;
        var characters = new List<Character>();
        foreach (Transform t in MapTransform)
        {
            characters.Add(t.GetComponent<Character>());
        }
        return characters;
    }

    public void highlightColorCheckBox(Character creater)
    {
        foreach (Character character in characters)
        {
            if (character.status != CharacterStatus.CheckBox || character.type != CharacterType.none) continue;
            character.highlightColorCheckBox(character.Creater == creater);
        }
    }

    public void normalColorCheckBox()
    {
        foreach (Character character in characters)
        {
            if (!character) continue;
            if (character.status != CharacterStatus.CheckBox || character.type != CharacterType.none) continue;
            character.highlightColorCheckBox(false);
        }
    }

    public Character checkCharacterAtPos(Vector3 Pos, CharacterType type)
    {
        return characters.Find(x => x && 
        new Vector2(Pos.x, Pos.z) == new Vector2(x.transform.position.x, x.transform.position.z)
        && x.status != CharacterStatus.CheckBox && x.type == type);
    }

    public (ResultMoveType type, Character parent) checkCanMove(Character character, Vector3 pos, bool preview)
    {
        var _characterArrow = characters.Find(x => x  && x.status == CharacterStatus.Arrow && x.type == CharacterType.Arrow
            && new Vector3(x.transform.position.x, character.transform.position.y, x.transform.position.z) == character.transform.position);
        //if (_characterArrow)
        //{
        //    if (character.ConvertNextPosToArrowType(pos) != _characterArrow.arrowType)
        //    {
        //        return (ResultMoveType.Stop, _characterArrow);
        //    }
        //}
        var _nextCharacterArrow = characters.Find(x => x && x.status == CharacterStatus.Arrow && x.type == CharacterType.Arrow
                 && new Vector3(x.transform.position.x, pos.y, x.transform.position.z) == pos);
        if (_nextCharacterArrow)
        {
            if (character.ConvertNextPosToArrowType(pos) != _nextCharacterArrow.arrowType)
            {
                return (ResultMoveType.Stop, _characterArrow);
            }
        }

        var _character = characters.Find(x => x && x.gameObject.activeSelf &&
        x.Creater == character && x.status == CharacterStatus.CheckBox && x.type == CharacterType.none
        && new Vector3(x.transform.position.x, pos.y, x.transform.position.z) == pos);

        var ContainsMoveBox = characters.Find(x => x && x.gameObject.activeSelf
        && x.status == CharacterStatus.Move
        && new Vector3(x.transform.position.x, pos.y, x.transform.position.z) == pos);

        var ContainsCopyBox = characters.Find(x => x && x.gameObject.activeSelf
        && x.status == CharacterStatus.CopyBlock
        && new Vector3(x.transform.position.x, pos.y, x.transform.position.z) == pos);

        if (ContainsCopyBox && ContainsCopyBox.Child != character)
        {
            return (ResultMoveType.Stop, _character);
        }

        if (_character)
        {
            return (ResultMoveType.Moving, _character);
        }
        _character = characters.Find(x => x && x.Child == null && new Vector3(x.transform.position.x, pos.y, x.transform.position.z) == pos);
        if (!_character) return (ResultMoveType.Stop, null);
        if ((!_character.isCharacterBox || 
            _character.status == CharacterStatus.Stop ||
            _character.status == CharacterStatus.Blank ||
            _character.status == CharacterStatus.Arrow ||
            ((_character.status == CharacterStatus.Static || _character.status == CharacterStatus.Complete) && 
            checkMoveType == CheckMoveType.MoveOnCharacter))
            && !ContainsMoveBox) return (ResultMoveType.Moving, _character);

        //Check Correct Letter 
        //◆例(example)：【if (((int)_character.checkBoxTypes & 1 << 4) != 0)】で4がONかどうかを判定可能（You can check Bool4"True" that write）
        if (((int)_character.checkBoxTypes & 1 << (int)character.type) != 0) return (ResultMoveType.Complete, _character);

        if (_character.status == CharacterStatus.CheckBox && _character.type != character.type)
        {
            if (!preview) updateCountFailed();
            return (ResultMoveType.GoBack, _character);
        }
        if (_character.status == CharacterStatus.Teleport) return (ResultMoveType.Teleport, _character);
        return (ResultMoveType.Stop, _character);
    }

    public (bool canMove, Character _character) checkBlankCanMove(Vector3 pos)
    {
        var _characters = characters.FindAll(x => x && x.gameObject.activeSelf && new Vector3(x.transform.position.x, pos.y, x.transform.position.z) == pos);
        if (_characters.Count == 0 || _characters.Count >= 2) return (false, null);
        return (_characters[0].status == CharacterStatus.CheckBox && _characters[0].type == CharacterType.none, _characters[0]);
    }

    private void Update()
    {
        if (UtilityToucher.GetMouseDown() && !UtilityToucher.IsTouchOnUI() && !isStarted)
        {
            isStarted = true;
            Destroy(TapToStart);
        }

        bool isWin = true;
        foreach (Character character in characters)
        {
            if(character && character.isCharacterBox && 
                (character.status == CharacterStatus.CheckBox && character.type != CharacterType.none))
            {
                isWin = false;
                break;
            }
        }
        if (isWin && !isEnded) ShowResultPanel(true);
    }

    private void updateCountFailed()
    {
        countFailed++;
        DisplayLifeIndicator();
        if (countFailed == canFail)
        {
            ShowResultPanel(false);
        }
    }

    IEnumerator DisplayLifeIndicatorView()
    {
        UIRoot.CanFailDisplay.DisplayFailEffect(canFail - countFailed + 1);

        yield return new WaitForSeconds(0.5f);

        UIRoot.CanFailDisplay.LifeNow = canFail - countFailed;
        UIRoot.CanFailDisplay.UpdateRemainingLife();
    }

    private void DisplayLifeIndicator()
    {
        StartCoroutine(DisplayLifeIndicatorView());
    }

    public void ReloadScene()
    {
        Global.ReloadScene();
    }

    public void Replay()
    {
        ReloadScene();
        maxSDK.ShowInterstitialDelay(1f);
    }

    public void NextLevel()
    {
        Data.CurrentLevel++;
        if (Data._TotalLevel < Data.CurrentLevel) Data.CurrentLevel = 1;
        ReloadScene();
    }

    private void ShowResultPanel(bool isWin)
    {
        if (isEnded) return;
        isEnded = true;
        this.isWin = isWin;
        if (isWin)
        {
            StartCoroutine(Global.Delay(2f, () =>
            {
                 ResultPanel.SetActive(true);
                 ResultPanel.transform.GetChild(0).gameObject.SetActive(true);
            }));
            StartCoroutine(Global.Delay(0.5f, () =>
            {
                Camera.main.transform.GetChild(0).gameObject.SetActive(true);
            }));
            // show app review
            if (Data.CurrentLevel == 3)
            {
                StartCoroutine(Global.Delay(3f, ShowAppReview));
            }
            Data.CurrentLevel++;
            if (Data._TotalLevel < Data.CurrentLevel) Data.CurrentLevel = 1;
        }
        else
        {
            StartCoroutine(Global.Delay(1.5f, () =>
            {
                ResultPanel.SetActive(true);
                ResultPanel.transform.GetChild(1).gameObject.SetActive(true);
            }));
            StartCoroutine(DropCharacters());
        }
        maxSDK.ShowInterstitialDelay(3f);
    }

    private IEnumerator DropCharacters()
    {
        yield return new WaitForSeconds(0.5f);
        Destroy(plane);
        foreach (Character character in characters)
        {
            if (!character) continue;
            character.Drop(Random.Range(15f, 50f));
            yield return Random.Range(0.1f, 0.5f);
        }
    }

    public void Debug_NextStage()
    {
        if (!isStarted)
        {
            isStarted = true;
            Destroy(TapToStart);
        }
        ShowResultPanel(true);
    }

    private void CheckShowTutorial()
    {
        if (Data.CurrentLevel == 1)
        {
            GameObject tut = Instantiate(tutorial, Vector3.zero, Quaternion.identity);
            tut.transform.localScale = Vector3.one;
        }
        else
        {
//            tutorial.SetActive(false);
        }
    }

    private bool IsFirstGame()
    {
        return PlayerPrefs.HasKey(TUTORIAL_PREF) ? false : true;
    }

    private void ShowAppReview()
    {
#if UNITY_IOS
        UnityEngine.iOS.Device.RequestStoreReview();
#else
        if (AppReview.instance != null) AppReview.instance.Open();
#endif
    }

    public void TurnOfTutorialNextTime()
    {
        PlayerPrefs.SetInt(TUTORIAL_PREF, 1);
    }

    
}
