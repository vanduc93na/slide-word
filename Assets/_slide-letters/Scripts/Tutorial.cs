﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour
{
    [SerializeField] private Image slideImg;
    [SerializeField] private Image fingerImg;
    [SerializeField] private Image arrowImg;

    private bool isPress = false;


    void Update()
    {
        if (UtilityToucher.GetMouseDown() && !isPress)
        {
            StartCoroutine(StartFade());
            isPress = true;
        }
    }

    IEnumerator StartFade()
    {
        float alpha = 1f;
        for (int i = 0; i < 10; i++)
        {
            alpha -= 0.1f;
            slideImg.color = new Color(1, 1, 1, alpha);
            fingerImg.color = new Color(1, 1, 1, alpha);
            arrowImg.color = new Color(1, 1, 1, alpha);
            yield return new WaitForSeconds(0.05f);
        }

        GameManager.instance.TurnOfTutorialNextTime();
        gameObject.SetActive(false);
    }
}
