﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Settings : MonoBehaviour
{
    
    //Reference
    [SerializeField] Button soundBtn, vibrationBtn;
    [SerializeField] private Sprite soundOn, soundOff;
    [SerializeField] Button buttonPrivacy, buttonTerms;
    [SerializeField] private GameObject vibrateOff;

    //Integer
    [SerializeField] int TargetFrameRate = 60;

    private void Awake()
    {
        Application.targetFrameRate = TargetFrameRate;
    }

    private void Start()
    {
        soundBtn.gameObject.GetComponent<Image>().sprite = Data.IsSoundOn ? soundOn : soundOff;
        soundBtn.onClick.AddListener(OnSoundChange);
        vibrateOff.SetActive(!Data.IsVibrationOn);
    }

    public void OnSoundChange()
    {
        Data.IsSoundOn = !Data.IsSoundOn;
        Global._isSoundOn = Data.IsSoundOn;
        soundBtn.gameObject.GetComponent<Image>().sprite = Data.IsSoundOn ? soundOn : soundOff;
        play_sound_click();
    }

    public void OnVibrationChange()
    {
        Data.IsVibrationOn = !Data.IsVibrationOn;
        Global._isVibrationOn = Data.IsVibrationOn;
        if (Data.IsVibrationOn) Handheld.Vibrate();
        play_sound_click();
        vibrateOff.SetActive(!Data.IsVibrationOn);
    }
//
//    public void PrivacyPolicyButton()
//    {
//        play_sound_click();
//        Application.OpenURL("");
//    }
//
//    public void AppTermsButton()
//    {
//        play_sound_click();
//        Application.OpenURL("");
//    }

    public void Open()
    {
        if (SceneManager.GetActiveScene().name == "Main") Time.timeScale = 0;
        play_sound_click();
        transform.GetChild(0).gameObject.SetActive(true);
    }

    public void Close()
    {
        if (SceneManager.GetActiveScene().name == "Main") Time.timeScale = 1;
        play_sound_click();
        transform.GetChild(0).gameObject.SetActive(false);
    }

    public void play_sound_click()
    {
//        SoundManager.instance.PlaySoundOneShot(SoundName.SELECT);
    }
}
