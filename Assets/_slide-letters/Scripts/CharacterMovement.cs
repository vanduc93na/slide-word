﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HyperCasualTemplate;

public enum DirectionType
{
    UP,
    DOWN,
    LEFT,
    RIGHT
}

public class CharacterMovement : MonoBehaviour
{
    //Public
    public static CharacterMovement instance;
    public GameObject VFXBlickCharactersPrefab;
    public GameObject VFXMatched;
    public GameObject VFXWrong;
    [HideInInspector] public bool isPressed;
    [HideInInspector] public Character character;

    //Private
    private Vector3 prevPos;
    private float CamPosZ;
    private float scrollDistance = 0.4f;
    private bool isBlank;
    private bool canMove = true;
    private float lastTimeMove;
    private bool teleporting = false;
    private bool characterMoving = false;
    private bool AutoMoving = false;
    private bool Backing;
    private bool Completing;

    private void Awake()
    {
        instance = this;
    }

    private void Update()
    {
        if (GameManager.instance.isEnded)
        {
            isPressed = false;
            GameManager.instance.normalColorCheckBox();
            return;
        }
        if (UtilityToucher.GetMouseDown() && !isPressed && !UtilityToucher.IsTouchOnUI() && canMove)
        {
            RaycastHit hit;
            if (Physics.Raycast(Camera.main.ScreenPointToRay(UtilityToucher.GetMousePosition()), out hit))
            {
                if (hit.collider.CompareTag("Character") && !Completing)
                {
                    var _character = hit.collider.GetComponent<Character>();
                    isBlank = _character.status == CharacterStatus.Blank;
                    var isCopyBlock = _character.status == CharacterStatus.CopyBlock;
                    if (isBlank || isCopyBlock || _character.status == CharacterStatus.Move)
                    {
                        if (isCopyBlock)
                        {
                            _character.HideBlock(true);
                            character = _character.Child;
                            character.gameObject.SetActive(true);
                        }
                        else character = _character;
                        GameManager.instance.highlightColorCheckBox(character);
                        CamPosZ = Camera.main.transform.position.y - character.transform.position.y;
                        prevPos = getTouchPos();
                        isPressed = true;
                        var camZ = Mathf.Abs(Camera.main.transform.position.z);
                        scrollDistance = 0.4f - (camZ-1)*0.1f;
                        if (scrollDistance < 0.15f) scrollDistance = 0.15f;
				        var BlockMovement = ConfigData.GetValue<int>(Config.BlockMovement_SlideLetters);
                        scrollDistance *= 100f/BlockMovement;
                    }
                }
            }
        }
        if ((UtilityToucher.GetMouseUp() && isPressed) || 
            (character && character.status == CharacterStatus.Stop) || !canMove)
        {
            GameManager.instance.normalColorCheckBox();
            isPressed = false;
        }

        if (!isPressed || !canMove) return;

        var distance = getTouchPos() - prevPos;

        if (Mathf.Abs(distance.z) >= scrollDistance)
        {
            Vector3 nextPos = Vector3.zero;
            if (distance.z < 0f)
            {
                nextPos = GetNextPos(0f, -1f);
            }
            else
            {
                nextPos = GetNextPos(0f, 1f);
            }
            if (isBlank)
            {
                var info = GameManager.instance.checkBlankCanMove(nextPos);
                if (info.canMove) handleMoveBlank(info._character, nextPos);
            }
            else
            {
                if (Time.time - lastTimeMove >= 0.05f)
                {
                    var info = GameManager.instance.checkCanMove(character, nextPos, false);
                    if (!teleporting && !characterMoving && !AutoMoving && !Backing && !Completing) handleMovoment(info, nextPos);
                    lastTimeMove = Time.time;
                }
            }
            prevPos = getTouchPos();
        }
        else if (Mathf.Abs(distance.x) >= scrollDistance)
        {
            Vector3 nextPos = Vector3.zero;
            if (distance.x < 0f)
            {
                nextPos = GetNextPos(-1f, 0f);
            }
            else
            {
                nextPos = GetNextPos(1f, 0);
            }
            if (isBlank)
            {
                var info = GameManager.instance.checkBlankCanMove(nextPos);
                if (info.canMove) handleMoveBlank(info._character, nextPos);
            }
            else
            {
                if (Time.time - lastTimeMove >= 0.05f)
                {
                    var info = GameManager.instance.checkCanMove(character, nextPos, false);
                    if (!teleporting && !characterMoving && !AutoMoving && !Backing && !Completing) handleMovoment(info, nextPos);
                    lastTimeMove = Time.time;
                }
            }
            prevPos = getTouchPos();
        }
    }

    private void handleMoveBlank(Character _character, Vector3 nextPos)
    {
        if (_character.Child != null) return;
        _character.gameObject.SetActive(false);
        if (character.Parent)
        {
            character.Parent.Child = null;
            character.Parent.gameObject.SetActive(true);
        }
        character.Parent = _character;
        _character.Child = character;

        var dist = nextPos - character.transform.position;
        var dir = dist.x == 1f ? DirectionType.RIGHT :
            (dist.x == -1f ? DirectionType.LEFT : (dist.z == 1f ? DirectionType.UP : DirectionType.DOWN));
        Completing = true;
        StartCoroutine(Move(nextPos, () =>
        {
            StartCoroutine(MoveDown(nextPos, () => { Completing = false; }));
        }));
    }

    private (Vector3 nextPos, List<Vector3> othersPos) CalcNextPos(Vector3 prevPos)
    {
        var othersPos = new List<Vector3>();
        Vector3 nextPos = Vector3.zero;
        var currPos = character.transform.position;
        var dist = currPos - prevPos;
        var dir = dist.x == 1f ? DirectionType.RIGHT :
            (dist.x == -1f ? DirectionType.LEFT : (dist.z == 1f ? DirectionType.UP : DirectionType.DOWN));
        switch (dir)
        {
            case DirectionType.UP:
                nextPos = new Vector3(currPos.x, currPos.y, currPos.z + 1f);
                othersPos.Add(new Vector3(currPos.x + 1f, currPos.y, currPos.z));
                othersPos.Add(new Vector3(currPos.x - 1f, currPos.y, currPos.z));
                return (nextPos, othersPos);
            case DirectionType.DOWN:
                nextPos = new Vector3(currPos.x, currPos.y, currPos.z - 1f);
                othersPos.Add(new Vector3(currPos.x + 1f, currPos.y, currPos.z));
                othersPos.Add(new Vector3(currPos.x - 1f, currPos.y, currPos.z));
                return (nextPos, othersPos);
            case DirectionType.LEFT:
                nextPos = new Vector3(currPos.x - 1f, currPos.y, currPos.z);
                othersPos.Add(new Vector3(currPos.x, currPos.y, currPos.z + 1f));
                othersPos.Add(new Vector3(currPos.x, currPos.y, currPos.z - 1f));
                return (nextPos, othersPos);
            default:
                nextPos = new Vector3(currPos.x + 1f, currPos.y, currPos.z);
                othersPos.Add(new Vector3(currPos.x, currPos.y, currPos.z + 1f));
                othersPos.Add(new Vector3(currPos.x, currPos.y, currPos.z - 1f));
                return (nextPos, othersPos);
        }
    }

    private void handleMovoment((ResultMoveType type, Character parent) info, Vector3 nextPos)
    {
        if (character.status == CharacterStatus.Stop) return;
        var prevPos = character.transform.position;
        switch (info.type)
        {
            case ResultMoveType.Moving:
                initVFXBlick(character.transform.position);
                initVFXBlick(nextPos);
                Global.VibrationHaptic(4);
                StartCoroutine(Move(nextPos, ()=>
                {
                    var AutoMove = ConfigData.GetValue<bool>(Config.AutoMove_SlideLetters);
                    if (AutoMove)
                    {
                        var infoPos = CalcNextPos(prevPos);
                        foreach (Vector3 otherPos in infoPos.othersPos)
                        {
                            var _infoOtherPos = GameManager.instance.checkCanMove(character, otherPos, true);
                            if (_infoOtherPos.type != ResultMoveType.Stop)
                            {
                                AutoMoving = false;
                                return;
                            }
                        }
                        var _nextPos = infoPos.nextPos;
                        var _info = GameManager.instance.checkCanMove(character, _nextPos, false);
                        handleMovoment(_info, _nextPos);
                        AutoMoving = _info.type == ResultMoveType.Moving;
                    }
                    else AutoMoving = false;
                }));
                if (character.Parent)
                {
                    character.Parent.gameObject.SetActive(true);
                    character.Parent.Child = null;
                }
                character.Parent = info.parent;
                info.parent.Child = character;
                if (info.parent.Creater == character) info.parent.gameObject.SetActive(false);
                break;
            case ResultMoveType.Complete:
                Completing = true;
                character.status = CharacterStatus.Stop;// always update to check if it is possible to keep moving
                if (character.Parent) character.Parent.Child = null;
                character.Parent = info.parent;
                info.parent.Child = character;
                info.parent.CheckCompleteWord();
                StartCoroutine(Move(nextPos, () =>
                {
                    initVFXBlick(character.transform.position);
                    initVFXBlick(nextPos);
                    initVFXMatched(nextPos);
                    Global.VibrationHaptic(5);
                    character.ChangeCharacterStatus(CharacterStatus.Stop);
                    StartCoroutine(MoveDown(nextPos, () => { Completing = false; }));
                }));
                break;
            case ResultMoveType.GoBack:
                Backing = true;
                initVFXWrong(nextPos);
                Global.VibrationHaptic(6);
                StartCoroutine(Move(nextPos, () =>
                {
                    StartCoroutine(Global.Delay(0.38f, () =>
                    {
                        if (GameManager.instance.isEnded) return;
                        StartCoroutine(Move(prevPos, null));
                        canMove = true;
                        Backing = false;
                        prevPos = getTouchPos();
                    }));
                }));
                canMove = false;
                break;
            case ResultMoveType.Teleport:
                teleporting = true;
                initVFXBlick(character.transform.position);
                initVFXBlick(nextPos);
                Global.VibrationHaptic(4);
                StartCoroutine(Move(nextPos, () =>
                {
                    if (info.parent.Connection.Child == character)
                    {
                        initVFXBlick(info.parent.Connection.transform.position);
                        StartCoroutine(handleTeleport(info.parent.Connection.transform.position));
                    }
                    else teleporting = false;
                }));
                if (character.Parent)
                {
                    character.Parent.gameObject.SetActive(true);
                    character.Parent.Child = null;
                }
                if (info.parent.Connection.Child == null)
                {
                    character.Parent = info.parent.Connection;
                    info.parent.Connection.Child = character;
                }
                else
                {
                    character.Parent = info.parent;
                    info.parent.Child = character;
                }
                break;
        }
    }

    private IEnumerator Move(Vector3 nextPos, System.Action callback)
    {
        characterMoving = true;
        var characterRot = character.transform.rotation.eulerAngles;
        var prevPos = character.transform.position;
        var info = getRotAsix(nextPos);
        var cubeface = character.GetCubeFace(info.dirType);
        character.transform.RotateAround(info.pos, info.asix, 90f);
        character.SetCubeFaceRot(cubeface);
        var backInfo = getRotAsix(prevPos);
        character.transform.RotateAround(backInfo.pos, backInfo.asix, 90f);

        var RollingBlock = ConfigData.GetValue<bool>(Config.RollingBlock_SlideLetters);
        if (RollingBlock)
        {
            var BlockSpeed = ConfigData.GetValue<int>(Config.BlockSpeed_SlideLetters) / 100f;
            var r = 0f;
            var step = 12f * BlockSpeed;
            var max = Mathf.CeilToInt(90f / step);
            for (int i = 0; i < max; i++)
            {
                if (i == max - 1) step = 90f - r;
                r += step;
                character.transform.RotateAround(info.pos, info.asix, step);
                yield return new WaitForSeconds(0.01f);
            }
            character.transform.position = nextPos;
            if (callback != null) callback();
        }
        else
        {
            StartCoroutine(NormalMove(nextPos, () =>
            {
                character.transform.RotateAround(info.pos, info.asix, 90f);
                character.transform.position = nextPos;
                if (callback != null) callback();
            }));
        }
        characterMoving = false;
    }

    private IEnumerator NormalMove(Vector3 nextPos, System.Action callback)
    {
        var timeStamp = Time.time;
        var BlockSpeed = ConfigData.GetValue<int>(Config.BlockSpeed_SlideLetters) / 100f;
        var duration = 0.12f / BlockSpeed;
        var prevPos = character.transform.position;
        while (Time.time - timeStamp <= duration)
        {
            var p = (Time.time - timeStamp) / duration;
            character.transform.position = Vector3.Lerp(prevPos, nextPos, p);
            yield return null;
        }
        if (callback != null) callback();
    }

    public (DirectionType dirType, Vector3 pos, Vector3 asix) getRotAsix(Vector3 nextPos)
    {
        var dist = nextPos - character.transform.position;
        var dir = dist.x > 0f ? DirectionType.RIGHT :
            (dist.x < 0f ? DirectionType.LEFT : (dist.z > 0f ? DirectionType.UP : DirectionType.DOWN));
        var currPos = character.transform.position;
        var pos = Vector3.zero;
        var asix = Vector3.zero;
        switch (dir)
        {
            case DirectionType.UP:
                pos = new Vector3(currPos.x, currPos.y - 0.5f, currPos.z + 0.5f);
                asix = new Vector3(1f, 0f, 0f);
                break;
            case DirectionType.DOWN:
                pos = new Vector3(currPos.x, currPos.y - 0.5f, currPos.z - 0.5f);
                asix = new Vector3(-1f, 0f, 0f);
                break;
            case DirectionType.LEFT:
                pos = new Vector3(currPos.x - 0.5f, currPos.y - 0.5f, currPos.z);
                asix = new Vector3(0f, 0f, 1f);
                break;
            case DirectionType.RIGHT:
                pos = new Vector3(currPos.x + 0.5f, currPos.y - 0.5f, currPos.z);
                asix = new Vector3(0f, 0f, -1f);
                break;
        }
        return (dir, pos, asix);
    }

    private IEnumerator MoveDown(Vector3 nextPos, System.Action callback)
    {
        nextPos.y = 0f;
        var rateTime = 0.125f;
        var timeStamp = Time.time;
        var prevPos = character.transform.position;
        while (Time.time - timeStamp < rateTime)
        {
            character.transform.position = Vector3.Lerp(prevPos, nextPos, (Time.time - timeStamp) / rateTime);
            yield return null;
        }
        character.transform.position = nextPos;
        if (callback != null) callback();
    }

    private IEnumerator handleTeleport(Vector3 nextPos)
    {
        canMove = false;
        nextPos.y = 1f;
        var rotateTimes = 2;
        var timeStamp = Time.time;
        var duration = 0.35f;
        var prevRot = character.transform.localRotation;
        while (Time.time - timeStamp < duration)
        {
            var p = (Time.time - timeStamp) / duration;
            character.transform.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, p);
            var newRot = Quaternion.Euler(new Vector3(0f, 1f, 0f) * Mathf.Lerp(0f, 360f, (p % (1f / rotateTimes)) * rotateTimes));
            character.transform.localRotation = newRot;
            yield return null;
        }

        timeStamp = Time.time;
        character.transform.position = nextPos;
        while (Time.time - timeStamp < duration)
        {
            var p = (Time.time - timeStamp) / duration;
            if (p <= 0.6f)
            {
                character.transform.localScale = Vector3.Lerp(Vector3.zero, new Vector3(1.2f, 1.2f, 1.2f), p / 0.6f);
            }
            else
            {
                character.transform.localScale = Vector3.Lerp(new Vector3(1.2f, 1.2f, 1.2f), Vector3.one, (p - 0.6f) / 0.4f);
            }
            var newRot = Quaternion.Euler(new Vector3(0f, 1f, 0f) * Mathf.Lerp(0f, 360f, (p % (1f / rotateTimes)) * rotateTimes));
            character.transform.localRotation = newRot;
            yield return null;
        }
        character.transform.localScale = Vector3.one;
        character.transform.localRotation = prevRot;
        teleporting = false;
        canMove = true;
    }

    private void initVFXBlick(Vector3 pos)
    {
        pos.y = 1f;
        var vfx = Instantiate(VFXBlickCharactersPrefab, pos, Quaternion.identity);
        StartCoroutine(Global.Delay(1f, () => { vfx.GetComponentInChildren<ParticleSystem>().Stop(true, ParticleSystemStopBehavior.StopEmitting); }));
        Destroy(vfx, 2f);
    }

    private void initVFXMatched(Vector3 pos)
    {
        pos.y = 0f;
        var vfx = Instantiate(VFXMatched, pos, Quaternion.identity);
        vfx.transform.GetChild(0).GetChild(1).GetComponent<ParticleSystemRenderer>().material.mainTexture = character.getMainTex();
        vfx.transform.GetChild(0).GetChild(2).GetComponent<ParticleSystemRenderer>().material.mainTexture = character.getMainTex();
        Destroy(vfx, 3f);
    }

    private void initVFXWrong(Vector3 pos)
    {
        pos.y = 1.5f;
        var vfx = Instantiate(VFXWrong, pos, Quaternion.identity);
        Destroy(vfx, 2f);
    }

    private Vector3 getTouchPos()
    {
        var touchPos = UtilityToucher.GetMousePosition();
        Vector3 currPos = Camera.main.ScreenToWorldPoint(new Vector3(touchPos.x, touchPos.y, CamPosZ));
        return currPos;
    }

    private Vector3 GetNextPos(float x, float z)
    {
        return new Vector3(
            character.transform.position.x + x,
            character.transform.position.y,
            character.transform.position.z + z);
    }
}
