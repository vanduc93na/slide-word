﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public enum CharacterType
{
    none,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,Teleport,Arrow
}

public enum CheckBoxTypes
{
    #region ListCheckBoxType
    none = 1 << 0,
    A = 1 << 1,
    B = 1 << 2,
    C = 1 << 3,
    D = 1 << 4,
    E = 1 << 5,
    F = 1 << 6,
    G = 1 << 7,
    H = 1 << 8,
    I = 1 << 9,
    J = 1 << 10,
    K = 1 << 11,
    L = 1 << 12,
    M = 1 << 13,
    N = 1 << 14,
    O = 1 << 15,
    P = 1 << 16,
    Q = 1 << 17,
    R = 1 << 18,
    S = 1 << 19,
    T = 1 << 20,
    U = 1 << 21,
    V = 1 << 22,
    W = 1 << 23,
    X = 1 << 24,
    Y = 1 << 25,
    Z = 1 << 26,
    Teleport = 1 << 27
    #endregion
}

public enum CharacterStatus
{
    None,
    Static,
    Blank,
    Move,
    Stop,
    Complete,
    CheckBox,
    Teleport,
    CopyBlock,
    Arrow
}

public enum CubeFaces
{
    TOP,
    BOTTOM,
    FORWARD,
    BACKWARD,
    LEFT,
    RIGHT
}

public enum ArrowType
{
    UP,
    DOWN,
    LEFT,
    RIGHT
}

[ExecuteInEditMode]
public class Character : MonoBehaviour
{
    //Public
    [SerializeField] public bool isCharacterBox;
    [SerializeField] public CharacterType type;
    [SerializeField] public CheckBoxTypes checkBoxTypes;
    [SerializeField] public CharacterStatus status;
    [SerializeField] public ArrowType arrowType;
    [SerializeField] public List<Character> Connections;
    [SerializeField] public Character Connection;
    [HideInInspector] public Character Parent;
    [HideInInspector] public Character Child;
    [HideInInspector] public Character Creater;
    [HideInInspector] public bool isCompletedWord;

    //Private
    private ArrowType prevArrowType;
    private List<Transform> faces;
    private List<Character> Children = new List<Character>();
    private Material characterMat;
    private Material ColorMat;
    private bool isHighLight;
    private bool isEnded;

    private void Start()
    {
        //createCheckBox();
        createMoveBox();
        copyMatCharacter();
        changeCharacterBoxStatus();
        SetCubeFaceRot(CubeFaces.TOP);
        createStaticBox();
        changeBoxRot();
        if (isCharacterBox) name = "Character_" + type + "_" + status;
        else name = "Character_" + type;
    }

    private void createCheckBox()
    {
        if (!isCharacterBox || status != CharacterStatus.Move || !Application.isPlaying) return;
        var characterClone = Instantiate(gameObject, transform.parent);
        var _character = characterClone.GetComponent<Character>();
        _character.type = CharacterType.none;
        _character.status = CharacterStatus.CheckBox;
        _character.Parent = null;
        _character.Child = _character.Creater = this;
        Parent = _character;
        Creater = this;
        _character.gameObject.SetActive(false);
        GameManager.instance.getCharacters();
    }

    private void createMoveBox()
    {
        if (!isCharacterBox || status != CharacterStatus.CopyBlock || !Application.isPlaying) return;
        var characterClone = Instantiate(gameObject, transform.parent);
        var _character = characterClone.GetComponent<Character>();
        _character.type = type;
        _character.status = CharacterStatus.Move;
        _character.Parent = null;
        _character.Creater = this;
        _character.gameObject.SetActive(false);
        Child = _character;
        GameManager.instance.getCharacters();
        Children.Add(_character);
    }

    public void createStaticBox()
    {
        if (!isCharacterBox || status != CharacterStatus.Blank || !Application.isPlaying) return;
        var characterClone = Instantiate(gameObject, transform.parent);
        var _character = characterClone.GetComponent<Character>();
        _character.isCharacterBox = false;
        _character.type = CharacterType.none;
        _character.status = CharacterStatus.None;
        _character.Child = _character.Parent = null;
        GameManager.instance.getCharacters();
    }

    private void copyMatCharacter()
    {
        var meshRenderers = transform.GetChild(0).GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer meshRenderer in meshRenderers)
        {
            var mats = new List<Material>(meshRenderer.sharedMaterials);
            var mat = mats[mats.Count - 1];
            if (mat == null)
            {
                //TextMaterial
                if(status == CharacterStatus.Move)
                {
                    mat = Resources.Load<Material>("Materials/CharacterMoveMat");
                }
                else if (status == CharacterStatus.Static)
                {
                    mat = Resources.Load<Material>("Materials/CharacterMat");
                }
                else
                {
                    mat = Resources.Load<Material>("Materials/CharacterMat");
                }
            }
            if (!characterMat)
            {
                characterMat = new Material(mat.shader);
                characterMat.CopyPropertiesFromMaterial(mat);
                loadCharacterTex();
            }
            mats[mats.Count - 1] = characterMat;
            meshRenderer.materials = mats.ToArray();
        }
    }

    public Texture getMainTex()
    {
        return characterMat.mainTexture;
    }

    public void loadCharacterTex(bool isNone = false)
    {
        string _type = status == CharacterStatus.CheckBox || isNone ? "none" : type.ToString();
        Texture2D tex = Resources.Load<Texture2D>("Characters/" + _type);
        if (type == CharacterType.Teleport) characterMat.SetColor("_Color", Color.white);
        else if (type == CharacterType.Arrow) characterMat.SetColor("_Color", Color.red);
        else characterMat.SetColor("_Color", Color.black);
        if (characterMat) characterMat.SetTexture("_MainTex", tex);
    }

    private void setCharacterColorMat(string matName)
    {
        var mat = Resources.Load<Material>("Materials/" + matName);
        var meshRenderers = transform.GetChild(0).GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer meshRenderer in meshRenderers)
        {
            var mats = new List<Material>(meshRenderer.sharedMaterials);
            mats[0] = mat;
            meshRenderer.materials = mats.ToArray();
        }
    }

    private void setCharacterColorMatText(string matName)
    {
        var mat = Resources.Load<Material>("Materials/" + matName);
        var meshRenderers = transform.GetChild(0).GetComponentsInChildren<MeshRenderer>();

        Texture tex = getMainTex();

        foreach (MeshRenderer meshRenderer in meshRenderers)
        {
            var mats = new List<Material>(meshRenderer.materials);
            
            if(mats.Count == 1)
            {
                mats.Add(mat);
            }
            else
            {
                mats[1] = mat;
            }
            meshRenderer.materials = mats.ToArray();
            meshRenderer.materials[1].SetTexture("_MainTex", tex);
        }
    }

    public void SetCubeFaceRot(CubeFaces face)
    {
        if (status == CharacterStatus.Arrow) return;
        if (faces == null)
        {
            faces = new List<Transform>();
            faces = new List<Transform>(transform.GetChild(0).GetComponentsInChildren<Transform>(true));
            faces.Remove(transform.GetChild(0));
        }
        var faceTransform = faces.Find(x => x.name == face.ToString());
        switch (face)
        {
            case CubeFaces.TOP:
                faceTransform.rotation = Quaternion.Euler(-90f, 0f, -90f);
                break;
            case CubeFaces.BOTTOM:
                faceTransform.rotation = Quaternion.Euler(90f, -90f, 0f);
                break;
            case CubeFaces.FORWARD:
                faceTransform.rotation = Quaternion.Euler(0f, -90f, 0f);
                break;
            case CubeFaces.BACKWARD:
                faceTransform.rotation = Quaternion.Euler(0f, 90f, 180f);
                break;
            case CubeFaces.LEFT:
                faceTransform.rotation = Quaternion.Euler(0f, 0f, -90f);
                break;
            case CubeFaces.RIGHT:
                faceTransform.rotation = Quaternion.Euler(0f, -90f, 90f);
                break;
        }
    }

    public CubeFaces GetCubeFace(DirectionType type)
    {
        Transform faceTransform = null;
        List<Vector3> facesPosition = new List<Vector3>();
        foreach (Transform _faceTransform in faces)
        {
            if (!_faceTransform.GetComponent<MeshRenderer>()) continue;
            facesPosition.Add(_faceTransform.GetComponent<MeshRenderer>().bounds.center);
        }

        int maxIndex = 0;
        faceTransform = faces[0];
        switch (type)
        {
            case DirectionType.UP:
                for (int i = 1; i < facesPosition.Count; i++)
                {
                    if (facesPosition[i].z < facesPosition[maxIndex].z)
                    {
                        maxIndex = i;
                        faceTransform = faces[i];
                    }
                }
                break;
            case DirectionType.DOWN:
                for (int i = 1; i < facesPosition.Count; i++)
                {
                    if (facesPosition[i].z > facesPosition[maxIndex].z)
                    {
                        maxIndex = i;
                        faceTransform = faces[i];
                    }
                }
                break;
            case DirectionType.LEFT:
                for (int i = 1; i < facesPosition.Count; i++)
                {
                    if (facesPosition[i].x > facesPosition[maxIndex].x)
                    {
                        maxIndex = i;
                        faceTransform = faces[i];
                    }
                }
                break;
            case DirectionType.RIGHT:
                for (int i = 1; i < facesPosition.Count; i++)
                {
                    if (facesPosition[i].x < facesPosition[maxIndex].x)
                    {
                        maxIndex = i;
                        faceTransform = faces[i];
                    }
                }
                break;
        }
        return (CubeFaces)System.Enum.Parse(typeof(CubeFaces), faceTransform.name);
    }

    public void changeCharacterBoxStatus()
    {
        float PosY = 0;
        string matName = null;
        string textMatName = null;
        switch (status)
        {
            case CharacterStatus.None:
                matName = "CharacterColorMat01";
                break;
            case CharacterStatus.Static:
                matName = "CharacterColorMat01";
                break;
            case CharacterStatus.Arrow:
                matName = "CharacterColorMat01";
                textMatName = "CharacterArrowMat";
                PosY = 0;
                break;
            case CharacterStatus.Move:
                PosY = 1f;
                matName = "CharacterColorMat02";
                textMatName = "CharacterMoveMat";
                break;
            case CharacterStatus.Stop:
                PosY = 1;
                if (Parent.isCompletedWord)
                {
                    matName = "CharacterColorMat06";
                    textMatName = "CharacterStopMat";
                }
                else
                {
                    matName = "CharacterColorMat02";
                    textMatName = "CharacterMoveMat";
                }
                break;
            case CharacterStatus.Blank:
                PosY = 1;
                matName = "CharacterColorMat05";
                break;
            case CharacterStatus.Complete:
                PosY = 0;
                matName = "CharacterColorMat06";
                textMatName = "CharacterCompleteMat";
                break;
            case CharacterStatus.CheckBox:
                PosY = -1f;
                matName = "CharacterColorMat04";
                break;
            case CharacterStatus.Teleport:
                PosY = 0f;
                matName = "CharacterColorMat07";
                textMatName = "CharacterTeleportMat";
                break;
            case CharacterStatus.CopyBlock:
                PosY = 1f;
                matName = "CharacterColorMat08";
                textMatName = "CharacterCopyBlockMat";
                break;
        }
        transform.position = new Vector3(transform.position.x, PosY, transform.position.z);
        if (!isHighLight)setCharacterColorMat(matName);
        if (textMatName != null) setCharacterColorMatText(textMatName);
        loadCharacterTex();
        changeBoxRot();
    }

    public void SetCharacterStatusIsStop()
    {
        status = CharacterStatus.Stop;
        setCharacterColorMat("CharacterColorMat06");
        setCharacterColorMatText("CharacterStopMat");
        loadCharacterTex();
        changeBoxRot();
    }


    private void changeBoxRot()
    {
        if (status != CharacterStatus.Arrow)
        {
            if (!Application.isPlaying) transform.localRotation = Quaternion.Euler(0f, -90f, 0f);
            return;
        }
        switch (arrowType)
        {
            case ArrowType.UP:
                transform.localRotation = Quaternion.Euler(0f, -90f, 0f);
                break;
            case ArrowType.DOWN:
                transform.localRotation = Quaternion.Euler(0f, 90f, 0f);
                break;
            case ArrowType.LEFT:
                transform.localRotation = Quaternion.Euler(0f, 180f, 0f);
                break;
            case ArrowType.RIGHT:
                transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
                break;
        }
        transform.GetChild(0).Find("TOP").localRotation = Quaternion.Euler(-90f, 0f, -90f);
    }

    public ArrowType ConvertNextPosToArrowType(Vector3 NextPos)
    {
        var dist = NextPos - transform.position;
        if (dist.x > 0) return ArrowType.RIGHT;
        if (dist.x < 0) return ArrowType.LEFT;
        if (dist.z > 0) return ArrowType.UP;
        return ArrowType.DOWN;
    }

    public void highlightColorCheckBox(bool isOn)
    {
        isHighLight = isOn;
        /*if (isOn)
        {
            setCharacterColorMat("CharacterColorMat05");
            return;
        }*/
        setCharacterColorMat("CharacterColorMat04");
    }

    public void fadeBoxColor(Character character, float Alpha)
    {
        var meshRenderers = character.transform.GetChild(0).GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer meshRenderer in meshRenderers)
        {
            var mats = new List<Material>(meshRenderer.sharedMaterials);
            if (!ColorMat)
            {
                ColorMat = new Material(mats[0].shader);
                ColorMat.CopyPropertiesFromMaterial(mats[0]);
            }
            mats[0] = ColorMat;
            mats[0].color = new Color(mats[0].color.r, mats[0].color.g, mats[0].color.b, Alpha);
            mats[1].color = new Color(mats[1].color.r, mats[1].color.g, mats[1].color.b, Alpha);
            meshRenderer.materials = mats.ToArray();
        }
    }

    public void ChangeCharacterStatus(CharacterStatus newStatus)
    {
        status = newStatus;
        changeCharacterBoxStatus();
    }

    public void CheckCompleteWord()
    {
        var horizontalWord = Connections.FindAll(x => x &&
        x.transform.position.x != transform.position.x &&
        x.transform.position.z == transform.position.z);
        var verticalWord = Connections.FindAll(x => x &&
        x.transform.position.x == transform.position.x &&
        x.transform.position.z != transform.position.z);

        string result = "";
        if (horizontalWord.Count > 0)
        {
            var MoveBlocks = new List<Character>();
            bool isCompleted = true;
            foreach (Character character in horizontalWord)
            {
                result += character.type.ToString();
                if (character.status == CharacterStatus.CheckBox)
                {
                    var MoveBlock = character.Child;
                    if (!MoveBlock)
                    {
                        isCompleted = false;
                        break;
                    }
                    MoveBlocks.Add(MoveBlock);
                }
            }
            if (isCompleted)
            {
                foreach (Character MoveBlock in MoveBlocks)
                {
                    Destroy(MoveBlock.Parent.gameObject);
                    MoveBlock.SetCharacterStatusIsStop();
                }
                CompleteWord(horizontalWord);
                isCompletedWord = true;
            }
        }
        if (verticalWord.Count > 0)
        {
            var MoveBlocks = new List<Character>();
            bool isCompleted = true;
            foreach (Character character in verticalWord)
            {
                result += character.type.ToString();
                if (character.status == CharacterStatus.CheckBox)
                {
                    var MoveBlock = character.Child;
                    if (!MoveBlock)
                    {
                        isCompleted = false;
                        break;
                    }
                    MoveBlocks.Add(MoveBlock);
                }
            }
            if (isCompleted)
            {
                foreach (Character MoveBlock in MoveBlocks)
                {
                    Destroy(MoveBlock.Parent.gameObject);
                    MoveBlock.SetCharacterStatusIsStop();
                }
                CompleteWord(verticalWord);
                isCompletedWord = true;
            }
        }
        Debug.Log(result);
    }

    public void CompleteWord(List<Character> characters)
    {
        characters.RemoveAll(x => x.status == CharacterStatus.CheckBox);
        if (Connections == null || Connections.Count == 0)
        {
            Destroy(gameObject);
            return;
        }
        foreach (Character character in characters)
        {
            character.ChangeCharacterStatus(CharacterStatus.Complete);
        }
        Destroy(gameObject);
    }

    public void Drop(float Speed)
    {
        if (!gameObject.activeSelf) return;
        if (status == CharacterStatus.CheckBox)
        {
            Destroy(gameObject);
            return;
        }
        StartCoroutine(_Drop(Speed));
    }

    public IEnumerator _Drop(float Speed)
    {
        var pos = transform.position;
        while (pos.y > -300f)
        {
            pos.y -= Speed * Time.deltaTime;
            transform.position = pos;
            yield return null;
        }
        Destroy(gameObject);
    }

    private IEnumerator AnimWakeup()
    {
        var rotateTimes = 2;
        var timeStamp = Time.time;
        var duration = 0.4f;
        var prevRot = transform.localRotation;
        var prevPos = transform.localPosition;
        bool isMoveDown = false;

        while (Time.time - timeStamp < duration)
        {
            var p = (Time.time - timeStamp) / duration;
            if (p <= 0.6f)
            {
                transform.localScale = Vector3.Lerp(Vector3.zero, new Vector3(1.2f, 1.2f, 1.2f), p / 0.6f);
                transform.localPosition = Vector3.Lerp(prevPos, new Vector3(prevPos.x, prevPos.y + 1f, prevPos.z), p / 0.6f);
            }
            else
            {
                if (!isMoveDown)
                {
                    transform.localRotation = prevRot;
                    timeStamp += 0.15f;
                    isMoveDown = true;
                    yield return new WaitForSeconds(0.15f);
                }
                transform.localScale = Vector3.Lerp(new Vector3(1.2f, 1.2f, 1.2f), Vector3.one, (p - 0.6f) / 0.4f);
                transform.localPosition = Vector3.Lerp(new Vector3(prevPos.x, prevPos.y +1f, prevPos.z), prevPos, (p - 0.6f) / 0.4f);
            }
            var newRot = Quaternion.Euler(new Vector3(0f, 1f, 0f) * Mathf.Lerp(0f, 360f, (p % (1f / rotateTimes)) * rotateTimes));
            transform.localRotation = newRot;
            yield return null;
        }
        transform.localPosition = prevPos;
        transform.localScale = Vector3.one;
        transform.localRotation = prevRot;
        if (GameManager.instance.isEnded) gameObject.SetActive(false);
    }

    private IEnumerator AnimFade()
    {
        var duration = 0.5f;
        var timeStamp = Time.time;

        List<Character> characters = new List<Character>();
        for (int i = 0; i < Children.Count; i++)
        {
            if (Children[i].status != CharacterStatus.Stop) characters.Add(Children[i]);
        }
        characters.Add(this);

        while (Time.time - timeStamp <= duration)
        {
            var alpha = 1f - ((Time.time - timeStamp) / duration);
            foreach (Character character in characters)
            {
                fadeBoxColor(character, alpha);
            }
            yield return null;
        }
        foreach (Character character in characters)
        {
            character.gameObject.SetActive(false);
        }
    }

    public void HideBlock(bool hide)
    {
        transform.GetChild(0).gameObject.SetActive(!hide);
    }

    private void Update()
    {
        if (status == CharacterStatus.Arrow)
        {
            if (prevArrowType != arrowType)
            {
                changeBoxRot();
                prevArrowType = arrowType;
            }
        }

        if (!Application.isPlaying || status != CharacterStatus.CopyBlock) return;

        if (new Vector2(transform.position.x, transform.position.z) !=
            new Vector2(Child.transform.position.x, Child.transform.position.z))
        {
            Child.Child = null;
            HideBlock(false);
            createMoveBox();
            StartCoroutine(AnimWakeup());
        }
        if (!CharacterMovement.instance.isPressed &&
            Child.gameObject.activeSelf &&
            new Vector2(transform.position.x, transform.position.z) ==
            new Vector2(Child.transform.position.x, Child.transform.position.z))
        {
            HideBlock(false);
            Child.gameObject.SetActive(false);
        }
        if (!isEnded && GameManager.instance.isEnded && GameManager.instance.isWin)
        {
            StartCoroutine(AnimFade());
            isEnded = true;
        }
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(Character))]
public class CharacterCutomInspector : Editor
{
    public override void OnInspectorGUI()
    {
        var character = (Character)target;
        var isCharacterBox = EditorGUILayout.Toggle("Character Box", character.isCharacterBox);
        if (character.isCharacterBox != isCharacterBox)
        {
            character.isCharacterBox = isCharacterBox;
            EditorUtility.SetDirty(target);
        }
        if (isCharacterBox)
        {
            EditorGUILayout.BeginVertical("ProgressBarBack");
            EditorGUI.indentLevel++;
            var type = (CharacterType)EditorGUILayout.EnumPopup("Type", character.type);
            if (type != character.type)
            {
                character.type = type;
                character.name = "Character_" + type;
                character.loadCharacterTex();
                EditorUtility.SetDirty(target);
            }
            var status = (CharacterStatus)EditorGUILayout.EnumPopup("Status", character.status);
            if (character.status != status)
            {
                character.status = status;
                character.changeCharacterBoxStatus();
                EditorUtility.SetDirty(target);
            }
            if (status == CharacterStatus.CheckBox)
            {
                var displayCheckBoxTypes = target as Character;
                var _preType = displayCheckBoxTypes.checkBoxTypes;
                displayCheckBoxTypes.checkBoxTypes = (CheckBoxTypes)EditorGUILayout.EnumFlagsField("CheckBoxTypes", displayCheckBoxTypes.checkBoxTypes);
                SerializedObject so = new SerializedObject(target);
                SerializedProperty stringsProperty = so.FindProperty("Connections");
                EditorGUILayout.PropertyField(stringsProperty, true);
                so.ApplyModifiedProperties();
                if (_preType != displayCheckBoxTypes.checkBoxTypes)EditorUtility.SetDirty(target);
            }
            if (status == CharacterStatus.Teleport)
            {
                SerializedProperty property = serializedObject.FindProperty("Connection");
                EditorGUILayout.PropertyField(property, true);
            }
            if (status == CharacterStatus.Arrow)
            {
                SerializedProperty property = serializedObject.FindProperty("arrowType");
                EditorGUILayout.PropertyField(property, true);
            }
            EditorGUI.indentLevel--;
            EditorGUILayout.EndVertical();
        }
        else
        {
            if (character.type != CharacterType.none)
            {
                character.type = CharacterType.none;
                character.status = CharacterStatus.None;
                character.loadCharacterTex();
                character.changeCharacterBoxStatus();
                EditorUtility.SetDirty(target);
            }
        }
        serializedObject.ApplyModifiedProperties();
    }
}
#endif