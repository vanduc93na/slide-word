﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    //Private
    private Camera _camera;
    private List<Character> characters;

    private void Start()
    {
        _camera = GetComponent<Camera>();
        characters = GameManager.instance.Characters();
        if (characters == null || characters.Count == 0) return;
        float Top = characters[0].transform.position.z;
        float Bottom = characters[0].transform.position.z;
        float Left = characters[0].transform.position.x;
        float Right = characters[0].transform.position.x;

        foreach (Character character in characters)
        {
            var characterTran = character.transform;
            if (characterTran.position.z > Top) Top = characterTran.position.z;
            if (characterTran.position.z < Bottom) Bottom = characterTran.position.z;
            if (characterTran.position.x < Left) Left = characterTran.position.x;
            if (characterTran.position.x > Right) Right = characterTran.position.x;
        }
        var CameraPos = new Vector3(
            Left + (Right - Left) / 2f, 
            transform.position.y, 
            Bottom + (Top - Bottom) / 2f);
        transform.position = CameraPos;

        var distSpace = new Vector2(1.2f, 4f);
        var CamTopLeft = GetCameraTopLeftPos();
        if (Mathf.Abs(CamTopLeft.x) - distSpace.x > Mathf.Abs(Left))
        {
            while (Mathf.Abs(CamTopLeft.x) - distSpace.x > Mathf.Abs(Left))
            {
                CamTopLeft = GetCameraTopLeftPos();
                transform.position = Vector3.MoveTowards(transform.position, transform.position + (transform.forward * 5f), 0.1f);
            }
        }
        if (Mathf.Abs(CamTopLeft.x) - distSpace.x < Mathf.Abs(Left))
        {
            while (Mathf.Abs(CamTopLeft.x) - distSpace.x < Mathf.Abs(Left))
            {
                CamTopLeft = GetCameraTopLeftPos();
                transform.position = Vector3.MoveTowards(transform.position, transform.position - (transform.forward * 5f), 0.1f);
            }
        }
        if (CamTopLeft.z <= Top + distSpace.y)
        {
            while (CamTopLeft.z <= Top + distSpace.y)
            {
                CamTopLeft = GetCameraTopLeftPos();
                if (CamTopLeft.z >= Top + distSpace.y) break;
                transform.position = Vector3.MoveTowards(transform.position, transform.position - (transform.forward * 5f), 0.1f);
            }
        }
    }

    private Vector3 GetCameraTopLeftPos()
    {
        Ray ray = _camera.ViewportPointToRay(new Vector3(0, 1, 0));
        Vector3 CamTopLeft = ray.origin + ((ray.origin.y - 0.5f) / -ray.direction.y * ray.direction);
        return CamTopLeft;
    }
}
