﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AppReview : MonoBehaviour
{
    public static AppReview instance;

    [SerializeField] private Image[] StarButtons;
    [SerializeField] private Sprite StarReview;
    [SerializeField] private Sprite StarReviewBG;
    [SerializeField] private GameObject ButtonNotNow;
    [SerializeField] private GameObject ButtonCancel;
    [SerializeField] private GameObject ButtonSubmit;
    [SerializeField] private int MinStarForSubmit;

//    [SerializeField] private string AndroidAppURL;
//    [SerializeField] private string IOSAppURL;
    private const string APP_RATING = "AppRating";
    private const string ANDROID_URL_PREFIX = "market://details?id=";

    private int AppRating
    {
        get
        {
            return PlayerPrefs.GetInt(APP_RATING, 0);
        }
        set
        {
            PlayerPrefs.SetInt(APP_RATING, value > 5 ? 5 : value);
            PlayerPrefs.Save();
        }
    }

    private void Awake()
    {
        instance = this;
        transform.GetChild(0).gameObject.SetActive(false);
    }

    private void UpdateStarButtons()
    {
        for (int i = 0; i < StarButtons.Length; i++)
        {
            StarButtons[i].sprite = i < AppRating ? StarReview : StarReviewBG;
        }
        if (AppRating >= 1)
        {
            ButtonCancel.SetActive(true);
            ButtonSubmit.SetActive(AppRating >= MinStarForSubmit);
            ButtonNotNow.SetActive(false);
        }
        else
        {
            ButtonCancel.SetActive(false);
            ButtonSubmit.SetActive(false);
            ButtonNotNow.SetActive(true);
        }
    }

    public void StarClick(int StarIndex)
    {
        AppRating = StarIndex;
        UpdateStarButtons();
        //SoundManager.instance.PlaySound_SELECT();
    }

    public void OpenStore()
    {
        string androidMarketURL = ANDROID_URL_PREFIX + Application.identifier;
        //SoundManager.instance.PlaySound_SELECT();
        Close();

#if UNITY_ANDROID
        Application.OpenURL(androidMarketURL);
#else
        Application.OpenURL(androidMarketURL);
#endif
    }

    public void Open()
    {
        transform.GetChild(0).gameObject.SetActive(true);
        UpdateStarButtons();
    }

    public void Close()
    {
        transform.GetChild(0).gameObject.SetActive(false);
        //Destroy(gameObject);
    }
}
