﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// this class define 
/// </summary>
[Serializable]
public class ItemCustomize
{
    public const string COLOR_CUSTOMIZE = "color_custom";
    public const string EFFECT_CUSTOMIZE = "effect_custom";
    public const string SOUND_CUSTOMIZE = "sound_custom";
    private const string COLOR_INDEX = "color_index";
    private const string EFFECT_INDEX = "effect_index";
    private const string SOUND_INDEX = "sound_index";
    private int currentColorIndexSelected = 0;
    private int currentEffectIndexSelected = 0;
    private int currentSoundIndexSelected = 0;

    // init list item is open
    public static void CheckInit(List<ColorCustomize> colors, List<EffectCustomize> effects, List<SoundCustomize> sounds)
    {
        if (!PlayerPrefs.HasKey(COLOR_CUSTOMIZE))
        {
            string data = "";
            for (int i = 0; i < colors.Count; i++)
            {
                data += i == 0 ? 1 : 0;
            }
            PlayerPrefs.SetString(COLOR_CUSTOMIZE, data);
            PlayerPrefs.SetInt(COLOR_INDEX, 0);
        }

        if (!PlayerPrefs.HasKey(EFFECT_CUSTOMIZE))
        {
            string data = "";
            for (int i = 0; i < effects.Count; i++)
            {
                data += i == 0 ? 1 : 0;
            }
            PlayerPrefs.SetString(EFFECT_CUSTOMIZE, data);
            PlayerPrefs.SetInt(EFFECT_INDEX, 0);
        }

        if (!PlayerPrefs.HasKey(SOUND_CUSTOMIZE))
        {
            string data = "";
            for (int i = 0; i < sounds.Count; i++)
            {
                data += i == 0 ? 1 : 0;
            }
            PlayerPrefs.SetString(SOUND_CUSTOMIZE, data);
            PlayerPrefs.SetInt(SOUND_INDEX, 0);
        }

    }

    /// <summary>
    /// get list open item from playerpref, it is isOpen variable in ItemCustomize
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public static List<bool> IsOpenItems(TypeOfCustomize type)
    {
        List<bool> result = new List<bool>();
        string data = "0";
        switch (type)
        {
            case TypeOfCustomize.color:
                data = PlayerPrefs.GetString(COLOR_CUSTOMIZE);
                break;
            case TypeOfCustomize.effect:
                data = PlayerPrefs.GetString(EFFECT_CUSTOMIZE);
                break;
            case TypeOfCustomize.sound:
                data = PlayerPrefs.GetString(SOUND_CUSTOMIZE);
                break;
        }

        for (int i = 0; i < data.Length; i++)
        {
            bool t = data[i] == '0' ? false : true;
            result.Add(t);
        }

        return result;
    }

    /// <summary>
    /// update isOpen variable
    /// </summary>
    /// <param name="values"></param>
    /// <param name="type"></param>
    public static void SaveItem(List<bool> values, TypeOfCustomize type)
    {
        string data = "";
        for (int i = 0; i < values.Count; i++)
        {
            string t = values[i] == true ? "1" : "0";
            data += t;
        }

        switch (type)
        {
            case TypeOfCustomize.color:
                PlayerPrefs.SetString(COLOR_CUSTOMIZE, data);
                break;
            case TypeOfCustomize.effect:
                PlayerPrefs.SetString(EFFECT_CUSTOMIZE, data);
                break;
            case TypeOfCustomize.sound:
                PlayerPrefs.SetString(SOUND_CUSTOMIZE, data);
                break;
        }
    }

    public static List<ColorCustomize> GetColorItems(List<ColorCustomize> color)
    {
        List<ColorCustomize> result = color;
        string data = PlayerPrefs.GetString(COLOR_CUSTOMIZE);
        for (int i = 0; i < result.Count; i++)
        {
            bool t = data[i] == '0' ? false : true;
            result[i].isOpen = t;
        }
        return result;
    }

    public static List<EffectCustomize> GetEffectItems(List<EffectCustomize> effect)
    {
        List<EffectCustomize> result = effect;
        string data = PlayerPrefs.GetString(COLOR_CUSTOMIZE);
        for (int i = 0; i < result.Count; i++)
        {
            bool t = data[i] == '0' ? false : true;
            result[i].isOpen = t;
        }
        return result;
    }

    public static List<SoundCustomize> GetSoundItems(List<SoundCustomize> sound)
    {
        List<SoundCustomize> result = sound;
        string data = PlayerPrefs.GetString(COLOR_CUSTOMIZE);
        for (int i = 0; i < result.Count; i++)
        {
            bool t = data[i] == '0' ? false : true;
            result[i].isOpen = t;
        }
        return result;
    }

    public static void SetColorSelected(int index)
    {
        PlayerPrefs.SetInt(COLOR_INDEX, index);
    }

    public static void SetEffectSelectedIndex(int index)
    {
        PlayerPrefs.SetInt(EFFECT_INDEX, index);
    }

    public static void SetSoundSelectedIndex(int index)
    {
        PlayerPrefs.SetInt(SOUND_INDEX, index);
    }

    public static int GetColorSelectedIndex()
    {
        return PlayerPrefs.GetInt(COLOR_INDEX);
    }

    public static int GetEffectSelectedIndex()
    {
        return PlayerPrefs.GetInt(EFFECT_INDEX);
    }

    public static int GetSoundSelectedIndex()
    {
        return PlayerPrefs.GetInt(SOUND_INDEX);
    }
}

[System.Serializable]
public class ColorCustomize
{
    public Color color = Color.white;
    public Sprite icon;
    public int price;
    public bool isOpen;
}

[System.Serializable]
public class EffectCustomize
{
    public Sprite icon;
    public GameObject gameObject;
    public int price;
    public bool isOpen;
}

[System.Serializable]
public class SoundCustomize
{
    public Sprite icon;
    public AudioClip audio;
    public int price;
    public bool isOpen;
}