﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoreGroup : MonoBehaviour
{
    public static StoreGroup Instance;

    [SerializeField] private List<ColorCustomize> colors;
    [SerializeField] private List<EffectCustomize> effect;
    [SerializeField] private List<SoundCustomize> sound;
    [Space(25)]
    [SerializeField] private GameObject storeCanvas;
    [SerializeField] private GameObject reviewItem;
    [SerializeField] private GameObject customize;
    [Space(25)] [SerializeField] private ItemsUI items;

    

    void Awake()
    {
        if (Instance == null) Instance = this;
    }

    void Start()
    {
        ItemCustomize.CheckInit(colors, effect, sound);
        storeCanvas.SetActive(false);
        reviewItem.SetActive(false);
        customize.SetActive(false);
    }

    void OnEnable()
    {
        
    }
    
    /// <summary>
    /// button selected type of customize
    /// </summary>
    /// <param name="value"></param>
    public void CustomizeClick(int value)
    {
        switch (value)
        {
            case 0:
                SetDataItems(TypeOfCustomize.color);
                break;
            case 1:
                SetDataItems(TypeOfCustomize.effect);
                break;
            case 2:
                SetDataItems(TypeOfCustomize.sound);
                break;
        }
    }

    void SetDataItems(TypeOfCustomize type)
    {
//        GameManager.instance.HideMapGamePlay();
        switch (type)
        {
            case TypeOfCustomize.color:
                items.SetColorItems(ItemCustomize.GetColorItems(colors));
                break;
            case TypeOfCustomize.effect:
                items.SetEffectItems(ItemCustomize.GetEffectItems(effect));
                break;
            case TypeOfCustomize.sound:
                items.SetSoundItems(ItemCustomize.GetSoundItems(sound));
                break;
        }
        storeCanvas.SetActive(true);
        reviewItem.SetActive(true);
        customize.SetActive(false);
    }

    public void ShowCustomize()
    {
        customize.SetActive(true);
    }

    public Color GetCurrentColorPicked()
    {
        return colors[ItemCustomize.GetColorSelectedIndex()].color;
    }

    public GameObject GetCurrentEffectPicked()
    {
        return effect[ItemCustomize.GetEffectSelectedIndex()].gameObject;
    }

    public AudioClip GetCurrentAudioPicked()
    {
        return sound[ItemCustomize.GetSoundSelectedIndex()].audio;
    }
    
}

public enum TypeOfCustomize
{
    color,
    effect,
    sound
}
