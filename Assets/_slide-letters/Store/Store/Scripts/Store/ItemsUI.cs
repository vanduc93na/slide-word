﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using HyperCasualTemplate;
using SRF;
using UnityEngine;
using UnityEngine.UI;

public class ItemsUI : MonoBehaviour
{
    [SerializeField] private GameObject review;

    [Space(25)]
    [SerializeField] private Button unlockWithCoinBtn;
    [SerializeField] private Button unlockWithAdsBtn;
    [SerializeField] private Sprite askItemSpr;
    [SerializeField] private Sprite selectedSpr;
    [SerializeField] private Sprite bgButtonLock;
    [SerializeField] private Sprite bgButtonUnLock;
    [Space(25)]
    [SerializeField] private List<Item> items;
    [SerializeField] private List<int> prices;

    private HyperCasualTemplate.AppLovinMaxSDK maxSDK;
    private List<ColorCustomize> _colors;
    private List<EffectCustomize> _effects;
    private List<SoundCustomize> _sounds;
    private TypeOfCustomize type;
    private int nextIndexPrice = 0;

    void Awake()
    {

    }

    void Start()
    {
        maxSDK = (HyperCasualTemplate.AppLovinMaxSDK)FindObjectOfType(typeof(HyperCasualTemplate.AppLovinMaxSDK));
    }
    
    public void SetColorItems(List<ColorCustomize> colors)
    {
        // count next index of prices
        int tmp = 0;
        for (int i = 0; i < colors.Count; i++)
        {
            if (colors[i].isOpen) tmp += 1;
        }
        nextIndexPrice = tmp - 1;

        _colors = colors;
        type = TypeOfCustomize.color;
        SetPriceButton(nextIndexPrice);
        for (int i = 0; i < items.Count; i++)
        {
            items[i].SetSelectedSprite(selectedSpr);
            if (i < colors.Count)
            {
                Sprite icon = colors[i].isOpen ? colors[i].icon : askItemSpr;
                if (colors[i].isOpen)
                {
                    items[i].SetIcon(icon);
                    items[i].SetIconColor(colors[i].color);
                    items[i].GetComponent<Button>().interactable = true;
                }
                else
                {
                    items[i].GetComponent<Button>().interactable = false;
                    items[i].SetIcon(askItemSpr);
                }
                items[i].gameObject.SetActive(true);
            }
            else
            {
                items[i].gameObject.SetActive(false);
            }
        }
    }

    public void SetEffectItems(List<EffectCustomize> effects)
    {
        _effects = effects;
        // count next index of prices
        int tmp = 0;
        for (int i = 0; i < effects.Count; i++)
        {
            if (effects[i].isOpen) tmp += 1;
        }
        nextIndexPrice = tmp - 1;
        SetPriceButton(nextIndexPrice);
        type = TypeOfCustomize.effect;
        for (int i = 0; i < items.Count; i++)
        {
            items[i].SetSelectedSprite(selectedSpr);
            if (i < effects.Count)
            {
                Sprite icon = effects[i].isOpen ? effects[i].icon : askItemSpr;
                if (effects[i].isOpen)
                {
                    items[i].SetIcon(icon);
                    items[i].GetComponent<Button>().interactable = true;
                }
                else
                {
                    items[i].SetIcon(askItemSpr);
                    items[i].GetComponent<Button>().interactable = false;
                }
                items[i].gameObject.SetActive(true);
            }
            else
            {
                items[i].gameObject.SetActive(false);
            }
        }
    }

    public void SetSoundItems(List<SoundCustomize> sounds)
    {
        _sounds = sounds;
        // count next index of prices
        int tmp = 0;
        for (int i = 0; i < sounds.Count; i++)
        {
            if (sounds[i].isOpen) tmp += 1;
        }
        nextIndexPrice = tmp - 1;
        SetPriceButton(nextIndexPrice);
        type = TypeOfCustomize.sound;
        for (int i = 0; i < items.Count; i++)
        {
            items[i].SetSelectedSprite(selectedSpr);
            if (i < sounds.Count)
            {
                Sprite icon = sounds[i].isOpen ? sounds[i].icon : askItemSpr;
                if (sounds[i].isOpen)
                {
                    items[i].SetIcon(icon);
                    items[i].GetComponent<Button>().interactable = true;
                }
                else
                {
                    items[i].SetIcon(askItemSpr);
                    items[i].GetComponent<Button>().interactable = false;
                }
                items[i].gameObject.SetActive(true);
            }
            else
            {
                items[i].gameObject.SetActive(false);
            }
        }
    }

    public void OnClick(int index)
    {
        for (int i = 0; i < items.Count; i++)
        {
            items[i].SetDeSelectedImage();
        }
        items[index].SetSelectedImage();
        switch (type)
        {
            case TypeOfCustomize.color:
                ItemCustomize.SetColorSelected(index);
//                GameManager.instance.SetBgColor();
                break;
            case TypeOfCustomize.effect:
                ItemCustomize.SetEffectSelectedIndex(index);
                review.transform.GetChild(0).GetChild(1).GetChild(2).DestroyChildren();
                var fx = Instantiate(StoreGroup.Instance.GetCurrentEffectPicked(), Vector3.one, Quaternion.identity);
                fx.transform.SetParent(review.transform.GetChild(0).GetChild(1).GetChild(2));
                fx.transform.localScale = Vector3.one;
                break;
            case TypeOfCustomize.sound:
                ItemCustomize.SetSoundSelectedIndex(index);
                break;
        }
    }

    public void UnlockWithCoin()
    {
        Data.CurrentCoin -= prices[nextIndexPrice];
        SetPriceButton(nextIndexPrice);
        StartCoroutine(UnlockItemAnimation());
    }

    public void UnlockWithAds()
    {
        maxSDK.ShowRewardedAd((bool isSucces) =>
        {
            if (isSucces)
            {
                Data.CurrentCoin += (int) ConfigData.GetValue<long>(Config.GetCoins_rewards_CubeLetters);
                SetPriceButton(nextIndexPrice);
            }
        });
    }

    IEnumerator UnlockItemAnimation()
    {
        int lastResult = 0;
        List<bool> updateValue = new List<bool>();
        unlockWithCoinBtn.transform.GetComponent<Image>().sprite = bgButtonLock;
        switch (type)
        {
            case TypeOfCustomize.color:
                for (int i = 0; i < 15; i++)
                {
                    int random = 0;
                    while (true)
                    {
                        random = Random.Range(1, _colors.Count);
                        if (!_colors[random].isOpen) break;
                    }
                    items[random].SetSelectedImage();
                    yield return new WaitForSeconds(0.2f);
                    items[random].SetDeSelectedImage();
                    lastResult = random;
                }
                _colors[lastResult].isOpen = true;
                items[lastResult].SetIcon(_colors[lastResult].icon);
                items[lastResult].SetIconColor(_colors[lastResult].color);
                items[lastResult].transform.GetComponent<Button>().interactable = true;
                for (int i = 0; i < _colors.Count; i++)
                {
                    updateValue.Add(_colors[i].isOpen);
                }
                
                break;
            case TypeOfCustomize.effect:
                for (int i = 0; i < 15; i++)
                {
                    int random = 0;
                    while (true)
                    {
                        random = Random.Range(1, _effects.Count);
                        if (!_effects[random].isOpen) break;
                    }
                    items[random].SetSelectedImage();
                    yield return new WaitForSeconds(0.2f);
                    items[random].SetDeSelectedImage();
                    lastResult = random;
                }
                _effects[lastResult].isOpen = true;
                items[lastResult].SetIcon(_effects[lastResult].icon);
                items[lastResult].transform.GetComponent<Button>().interactable = true;
                for (int i = 0; i < _effects.Count; i++)
                {
                    updateValue.Add(_effects[i].isOpen);
                }
                break;
            case TypeOfCustomize.sound:
                for (int i = 0; i < 15; i++)
                {
                    int random = 0;
                    while (true)
                    {
                        random = Random.Range(1, _sounds.Count);
                        if (!_sounds[random].isOpen) break;
                    }
                    items[random].SetSelectedImage();
                    yield return new WaitForSeconds(0.2f);
                    items[random].SetDeSelectedImage();
                    lastResult = random;
                }
                _sounds[lastResult].isOpen = true;
                items[lastResult].SetIcon(_sounds[lastResult].icon);
                items[lastResult].transform.GetComponent<Button>().interactable = true;
                for (int i = 0; i < _sounds.Count; i++)
                {
                    updateValue.Add(_sounds[i].isOpen);
                }
                break;
        }
        unlockWithCoinBtn.transform.GetComponent<Image>().sprite = bgButtonUnLock;
        nextIndexPrice += 1;
        SetPriceButton(nextIndexPrice);
        ItemCustomize.SaveItem(updateValue, type);
        

    }

    void SetPriceButton(int index)
    {
        unlockWithCoinBtn.transform.GetChild(2).GetComponent<Text>().text = prices[index].ToString();
        unlockWithAdsBtn.transform.GetChild(2).GetComponent<Text>().text =
            ConfigData.GetValue<long>(Config.GetCoins_rewards_CubeLetters).ToString();
        if (Data.CurrentCoin >= prices[index])
        {
            unlockWithCoinBtn.interactable = true;
            unlockWithCoinBtn.GetComponent<Image>().sprite = bgButtonUnLock;
        }
        else
        {
            unlockWithCoinBtn.interactable = false;
            unlockWithCoinBtn.GetComponent<Image>().sprite = bgButtonLock;
        }
    }

    public void OnClose()
    {
//        GameManager.instance.ShowMapGamePlay();
    }
}
