﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Item : MonoBehaviour
{
    
    [SerializeField] private Image bg;
    [SerializeField] private Image icon;
    private Sprite deSelected;
    private Sprite selected;

    void Start()
    {
        GetComponent<Button>().onClick.AddListener(onClick);
        deSelected = GetComponent<Image>().sprite;
    }

    private void onClick()
    {
        int index = transform.GetSiblingIndex();
        transform.parent.GetComponent<ItemsUI>().OnClick(index);
    }

    public void SetBG(Sprite bg)
    {
        this.bg.sprite = bg;
    }

    public void SetIcon(Sprite icon)
    {
        this.icon.sprite = icon;
    }

    public void SetIconColor(Color color)
    {
        icon.color = color;
    }

    public void SetSelectedSprite(Sprite select)
    {
        this.selected = select;
    }

    public void SetDeSelectedImage()
    {
        GetComponent<Image>().sprite = deSelected;
    }

    public void SetSelectedImage()
    {
        GetComponent<Image>().sprite = selected;
    }
}
