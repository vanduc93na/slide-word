﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace  HyperCasualTemplate
{
    public enum Config 
    {
        // int sample (int value, sort priority, display name)
        [DefaultValue(15000, 200)]AdInterval,
        [DefaultValue(100, 200)]BlockMovement_SlideLetters,

        // bool sample (bool value, sort priority, display name)
        [DefaultValue(false, 210, "GDPR")]is_gdpr_countries,

        [DefaultValue(true, 220, "RollingBlock")] RollingBlock_SlideLetters,
        [DefaultValue(false, 230, "AutoMove")] AutoMove_SlideLetters,
        [DefaultValue(100, 240, "BlockSpeed")] BlockSpeed_SlideLetters,
        [DefaultValue(50, 250, "RemoteCoinReward")] GetCoins_inGame_CubeLetters,
        [DefaultValue(50, 250, "RemoteCoinReward")] GetCoins_rewards_CubeLetters
        // string sample (string value, sort priority, display name)
        //[DefaultValue("b1,b2", 220)]ChestBonusItemCategories,
    }
    
}
